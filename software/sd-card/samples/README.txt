Put your samples in this folder.

The filenames must be in 8.3 format (eg: "REDALERT.WAV")

The samples must be WAV files, in 16-bit PCM at 44.1 KHz

They can be either mono or stereo.