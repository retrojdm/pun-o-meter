////////////////////////////////////////////////////////////////////////////////
// PUN-O-METER - Core
// Andrew Wyatt - retrojdm.com


// Uncomment this line if you want to see publications on the serial monitor.
#define SERIAL_MONITOR_MODULE


////////////////////////////////////////////////////////////////////////////////
// Libraries

#include <Arduino.h>

// Publisher/Subscriber design pattern (event queue).
// https://bitbucket.org/retrojdm/ringbuffer
// https://bitbucket.org/retrojdm/pub-sub
#include <RingBuffer.h>
#include <PubSub.h>

// Audio
// https://www.pjrc.com/teensy/td_download.html
#include <Audio.h>
#include <SD.h>
#include <SerialFlash.h>
#include <SPI.h>
#include <Wire.h>

// Input
// https://github.com/thomasfredericks/Bounce2
#include <Bounce2.h>

// Settings
// https://github.com/bblanchon/ArduinoJson/
//
// Note:  We're using version 5.13.2
//        There were breaking changes in version 6.
//        eg: JsonBuffer was renamed JsonDocument
//
#include <ArduinoJson.h>

// Stepper
// https://github.com/clearwater/SwitecX25
//
// Note:  You may need to increase RESET_STEP_MICROSEC to 1500 for the stepper to
//        zero.
//
//        You may also need to increase the last few delay value in the
//        defaultAccelTable for the needle to move without skipping steps.
//
//        Eg:
//          static unsigned short defaultAccelTable[][2] = {
//            {   20, 3000},
//            {   50, 2000},
//            {  100, 1500},
//            {  150, 1200},
//            {  225, 1000}
//          };
#include <SwitecX25.h>


////////////////////////////////////////////////////////////////////////////////
// Main program

#include "src/_common/Enums/Origin.h"
#include "src/_common/Constants.h"
#include "src/Audio/AudioModule.h"
#include "src/Input/InputModule.h"
#include "src/PunLevel/PunLevelModule.h"
#include "src/Serial/SerialModule.h"
#ifdef SERIAL_MONITOR_MODULE
#include "src/SerialMonitor/SerialMonitorModule.h"
#endif
#include "src/Settings/SettingsModule.h"
#include "src/SignalLight/SignalLightModule.h"
#include "src/Stepper/StepperModule.h"

using namespace pom;



// This Broker object will take care of the global event bus and message queue.
Broker gBroker(kPublicationDataSize, kMaxPublications, kMaxSubscriptions);

AudioModule         gAudio        (&gBroker);
InputModule         gInput        (&gBroker);
PunLevelModule      gPunLevel     (&gBroker);
SerialModule        gSerial       (&gBroker);
#ifdef SERIAL_MONITOR_MODULE
SerialMonitorModule gSerialMonitor(&gBroker);
#endif
SettingsModule      gSettings     (&gBroker);
SignalLightModule   gSignalLight  (&gBroker);
StepperModule       gStepper      (&gBroker);

void setup()
{
  gBroker.begin();
  
  gAudio.begin();
  gInput.begin();
  gPunLevel.begin();
  gSerial.begin();
  #ifdef SERIAL_MONITOR_MODULE
  gSerialMonitor.begin();
  #endif
  gSettings.begin();
  gSignalLight.begin();
  gStepper.begin();
}


void loop()
{
  gBroker.distribute();

  gAudio.loop();
  gInput.loop();
  gPunLevel.loop();
  gSerial.loop();
  #ifdef SERIAL_MONITOR_MODULE
  gSerialMonitor.loop();
  #endif
  gSettings.loop();
  gSignalLight.loop();
  gStepper.loop();
}
