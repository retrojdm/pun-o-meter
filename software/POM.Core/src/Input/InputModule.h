#pragma once

#include <Bounce2.h>
#include <PubSub.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"
#include "../Input/Enums/InputButton.h"
#include "../Input/Enums/InputPot.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  const uint8_t kInputButtonPunPin          = 17;
  const uint8_t kInputButtonRemovePin       = 16;
  const uint8_t kInputPotVolumePin          = A1;
  const int16_t kInputButtonDebounceMillis  = 5;
  const int16_t kInputPotChangedThreshold   = 1024 / 64;


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class InputModule : public Publisher
  {
  public:
    InputModule (Broker * pBroker);
    void begin  ();
    void loop   ();

  private:
    Bounce    _button   [(uint8_t)InputButton::Count];
    uint8_t   _potPin   [(uint8_t)InputPot::Count];
    uint16_t  _potValue [(uint8_t)InputPot::Count];

    void _handleButtons       ();
    void _handlePotentiometers();
  };
}