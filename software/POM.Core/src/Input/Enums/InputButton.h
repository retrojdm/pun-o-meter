#pragma once

namespace pom
{
  enum class InputButton {
    AddPun = 0,
    RemovePun,

    Count
  };
}