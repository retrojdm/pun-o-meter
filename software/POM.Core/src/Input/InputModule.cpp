#include "InputModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  InputModule::InputModule(Broker * pBroker) :
    Publisher(pBroker),
    _button { Bounce(), Bounce() } // We need to init each element in the _button array.
  {
    
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void InputModule::begin()
  {
    _button[(uint8_t)InputButton::AddPun].attach(kInputButtonPunPin, INPUT_PULLUP);
    _button[(uint8_t)InputButton::AddPun].interval(kInputButtonDebounceMillis);

    _button[(uint8_t)InputButton::RemovePun].attach(kInputButtonRemovePin, INPUT_PULLUP);
    _button[(uint8_t)InputButton::RemovePun].interval(kInputButtonDebounceMillis);

    _potPin[(uint8_t)InputPot::Volume] = kInputPotVolumePin;
    _potValue[(uint8_t)InputPot::Volume] = 0;
  }


  void InputModule::loop()
  {
    _handleButtons();
    _handlePotentiometers();
  }


  void InputModule::_handleButtons()
  {
    for (uint8_t i = 0; i < (uint8_t)InputButton::Count; i++)
    {
      _button[i].update();
      
      if (_button[i].fell())
      {
        publish(Publication((uint8_t)Topic::Input, (uint8_t)Message::EventInputButtonPressed, (uint32_t)i));
      }
    }
  }


  void InputModule::_handlePotentiometers()
  {
    for (uint8_t i = 0; i < (uint8_t)InputPot::Count; i++)
    {
      uint16_t value = analogRead(_potPin[i]);
      if (abs(value - _potValue[i]) >= kInputPotChangedThreshold)
      {
        _potValue[i] = value;

        char indexValuePair[13];
        sprintf(indexValuePair, "%u%c%u", i, kSettingsDelimiter, value);
        publish(Publication((uint8_t)Topic::Input, (uint8_t)Message::EventInputPotChanged, indexValuePair));
      }
    }
  }
}