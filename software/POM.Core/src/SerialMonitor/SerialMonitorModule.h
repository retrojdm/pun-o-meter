#pragma once

#include <PubSub.h>

#include "../_common/Enums/Topic.h"
#include "../_common/Enums/Message.h"
#include "../_common/Constants.h"
#include "../SerialMonitor/MessageMeta.h"
#include "../SerialMonitor/TopicMeta.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  const uint8_t   kSerialMonitorMessageCount      = 24;
  const uint8_t   kSerialMonitorTopicCount        = 10;
  const char      kSerialMonitorStartupMessage[]  = "Pun-o-Meter: POM.Core\r\nSerial Monitor Ready\r\n\r\n";
  const char      kSerialMonitorUnknown[]         = "[unknown]";
  const uint32_t  kSerialMonitorBaudRate          = 9600;


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class SerialMonitorModule : public Subscriber
  {
    public:
      SerialMonitorModule (Broker * pBroker);
      void begin          ();
      void loop           ();
      void notify         (Publication publication);

    private:
      MessageMeta _messageMeta  [kSerialMonitorMessageCount];
      TopicMeta   _topicMeta    [kSerialMonitorTopicCount];

      MessageMeta * _findMessageMeta  (Message message);
      TopicMeta *   _findTopicMeta    (Topic topic);
      void          _paramToString    (char * buffer, Publication publication);
      void          _binaryToString   (char * buffer, uint16_t bits, uint8_t places);
  };
}