#include "MessageMeta.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  MessageMeta::MessageMeta(
    Message message,
    const char * type,
    const char * subject,
    const char * command)
  {
    this->message = message;
    this->type    = type;
    this->subject = subject;
    this->command = command;
  }
}