#pragma once

#include "../_common/Enums/Topic.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class TopicMeta
  {
    public:
      TopicMeta(Topic topic, const char * name);

      Topic         topic;
      const char *  name;
  };
}