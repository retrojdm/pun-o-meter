#include "SerialMonitorModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  SerialMonitorModule::SerialMonitorModule(Broker * pBroker) :
    Subscriber(pBroker),
    
    _messageMeta {
      MessageMeta(Message::RequestSerialMonitorDebugMessage,  "request", "debug", "message"),
      
      // POM.Core
      MessageMeta(Message::EventAudioSamplePlaying,           "event", "sample", "playing"),
      MessageMeta(Message::EventAudioSampleStopped,           "event", "sample", "stopped"),
      MessageMeta(Message::RequestAudioSamplePlay,            "request", "sample", "play"),
      
      MessageMeta(Message::EventInputButtonPressed,           "event", "button", "pressed"),
      MessageMeta(Message::EventInputPotChanged,              "event", "pot", "changed"),
      
      MessageMeta(Message::EventPunLevelChanged,              "event", "pun level", "changed"),
      MessageMeta(Message::EventPunLevelAlert,                "event", "pun level", "alert"),
      MessageMeta(Message::RequestPunAdd,                     "request", "pun", "add"),

      MessageMeta(Message::EventStepperTargetSet,             "event", "target", "set"),
      MessageMeta(Message::EventStepperTargetReached,         "event", "target", "reached"),

      MessageMeta(Message::RequestSettingSet,                 "request", "setting", "set"),
      MessageMeta(Message::EventSettingChanged,               "event", "setting", "changed"),
      MessageMeta(Message::RequestSettingsSave,               "request", "settings", "save"),
      MessageMeta(Message::RequestSettingsReset,              "request", "settings", "reset"),
      MessageMeta(Message::EventSettingsSamplesCleared,       "event", "samples", "cleared"),
      MessageMeta(Message::EventSettingsSampleAdded,          "event", "sample", "added"),

      MessageMeta(Message::EventSignalLightEnergised,         "event", "signal light", "energised"),

      // POM.API
      MessageMeta(Message::EventServerRunning,                "event", "server", "running"),
      MessageMeta(Message::EventServerRequest,                "event", "server", "request"),
      MessageMeta(Message::EventServerResponse,               "event", "server", "response"),

      MessageMeta(Message::EventLedSequenceChanged,           "event", "sequence", "changed"),
      
      MessageMeta(Message::EventWifiConnectionStatusChanged,  "event", "connection status", "changed"),
      MessageMeta(Message::EventWifiConnectionAttempt,        "event", "connection", "attempt")
    },

    _topicMeta {
      TopicMeta(Topic::All,         "All"),
      TopicMeta(Topic::Audio,       "Audio"),
      TopicMeta(Topic::Input,       "Input"),
      TopicMeta(Topic::PunLevel,    "PunLevel"),
      TopicMeta(Topic::Settings,    "Settings"),
      TopicMeta(Topic::SignalLight, "SignalLight"),
      TopicMeta(Topic::Stepper,     "Stepper"),

      TopicMeta(Topic::Led,         "Led"),
      TopicMeta(Topic::Server,      "Server"),
      TopicMeta(Topic::Wifi,        "Wifi")
    }
  {
    
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void SerialMonitorModule::begin()
  {
    Serial.begin(kSerialMonitorBaudRate);
    Serial.print(kSerialMonitorStartupMessage);
    subscribe((uint8_t)Topic::All);
  }


  void SerialMonitorModule::loop()
  {

  }


  void SerialMonitorModule::notify(Publication publication)
  {
    char param[kPublicationDataSize + 2]; // +2 for the quote marks if it's a string.

    MessageMeta * pMessageMeta = _findMessageMeta((Message)publication.message);
    const char * messageTypeName    = (pMessageMeta == nullptr) ? kSerialMonitorUnknown : pMessageMeta->type;
    const char * messageSubjectName = (pMessageMeta == nullptr) ? kSerialMonitorUnknown : pMessageMeta->subject;
    const char * messageCommandName = (pMessageMeta == nullptr) ? kSerialMonitorUnknown : pMessageMeta->command;

    TopicMeta * pTopicMeta = _findTopicMeta((Topic)publication.topic);
    const char * topicName = (pTopicMeta == nullptr) ? kSerialMonitorUnknown : pTopicMeta->name;

    _paramToString(param, publication);

    char buffer[161];
    sprintf(buffer, "%-10s%-15s%-20s%-20s%s",
      messageTypeName,
      topicName,
      messageSubjectName,
      messageCommandName,
      param
    );

    Serial.println(buffer);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  MessageMeta * SerialMonitorModule::_findMessageMeta(Message message)
  {
    for (uint8_t i = 0; i < kSerialMonitorMessageCount; i++)
    {
      if (_messageMeta[i].message == message)
      {
        return &(_messageMeta[i]);
      }
    }
    return nullptr; 
  }


  TopicMeta * SerialMonitorModule::_findTopicMeta(Topic topic)
  {
    for (uint8_t i = 0; i < kSerialMonitorTopicCount; i++)
    {
      if (_topicMeta[i].topic == topic)
      {
        return &(_topicMeta[i]);
      }
    }
    return nullptr; 
  }


  void SerialMonitorModule::_paramToString(char * buffer, Publication publication)
  {
    switch (publication.dataType)
    {
    case kPublicationDataTypeBitSequence:
      _binaryToString(buffer, publication.getUint(), 16);
      return;

    case kPublicationDataTypeBool:
      strlcpy(buffer, publication.getBool() ? "true" : "false", kPublicationDataSize - 1);
      return;

    case kPublicationDataTypeInt:
      sprintf(buffer, "%ld", publication.getInt());
      return;  

    case kPublicationDataTypeUint:
      sprintf(buffer, "%lu", publication.getUint());
      return;

    case kPublicationDataTypeCharArray:
      sprintf(buffer, "\"%s\"", publication.getCharArray());
      return;

    case kPublicationDataTypeRawData:
      sprintf(buffer, "[data]");
      return;  

    // None
    default:
      buffer[0] = '\0';
      return;
    }
  }


  void SerialMonitorModule::_binaryToString(char * buffer, uint16_t bits, uint8_t places)
  {
    for (uint8_t x = 0; x < places; x++)
    {
      buffer[x] = ((bits >> x) & 1) ? 'x' : '-';
    }

    buffer[places] = '\0';
  }
}