#pragma once

#include "../_common/Enums/Message.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class MessageMeta
  {
    public:
      MessageMeta(Message message, const char * type, const char * subject, const char * command);

      Message       message;
      const char *  type;
      const char *  subject;
      const char *  command;
  };
}