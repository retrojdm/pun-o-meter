#include "TopicMeta.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  TopicMeta::TopicMeta(Topic topic, const char * name)
  {
    this->topic = topic;
    this->name  = name;
  }
}