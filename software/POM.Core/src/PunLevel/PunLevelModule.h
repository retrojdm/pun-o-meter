#pragma once

#include <PubSub.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"
#include "../Input/Enums/InputButton.h" // todo: Decouple button indexes from the Input Module?


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class PunLevelModule: public Publisher, public Subscriber
  {
  public:
    PunLevelModule  (Broker * pBroker);
    void begin      ();
    void loop       ();
    void notify     (Publication publication);

  private:
    // Settings
    uint8_t   _perPun;        // "pun units" (as indicated on the stepper gauge)
    uint8_t   _alarm;         // "pun units"
    uint16_t  _max;           // "pun units"
    uint16_t  _recoveryTime;  // millis

    // State
    uint16_t  _punLevel;      // "pun units"
    uint32_t  _lastDecayed;   // millis

    // Event handlers
    void  _onEventInputButtonPressed  (uint8_t buttonIndex);
    void  _onEventSettingChanged      (const char * sectionKeyValue);
    void  _onRequestPunAdd            (int8_t puns);

    // Private methods
    void _addPun                      (int8_t puns);
    void  _decay                      ();
  };
}