#include "PunLevelModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  PunLevelModule::PunLevelModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker)
  {
    // Settings
    _perPun       = kSettingsPunLevelPerPun;
    _alarm        = kSettingsPunLevelAlarm;
    _max          = kSettingsPunLevelMax;
    _recoveryTime = kSettingsPunLevelRecoveryTime;

    // State
    _punLevel     = 0;
    _lastDecayed  = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void PunLevelModule::begin()
  {
    subscribe((uint8_t)Topic::Input);
    subscribe((uint8_t)Topic::PunLevel);
    subscribe((uint8_t)Topic::Settings);
  }


  void PunLevelModule::loop()
  {
    uint32_t currentMillis = millis();
    if (currentMillis - _lastDecayed >= (uint32_t)((_recoveryTime * 60000) / _perPun))
    {
      _lastDecayed = currentMillis;
      _decay();
    }
  }


  void PunLevelModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventInputButtonPressed:  _onEventInputButtonPressed  (publication.getUint());      break;
      case Message::EventSettingChanged:      _onEventSettingChanged      (publication.getCharArray()); break;
      case Message::RequestPunAdd:            _onRequestPunAdd            (publication.getInt());       break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event Handlers

  void PunLevelModule::_onEventInputButtonPressed(uint8_t buttonIndex)
  {
    switch ((InputButton)buttonIndex)
    {
      case InputButton::AddPun:     _addPun(+1);  break;
      case InputButton::RemovePun:  _addPun(-1);  break;
    }
  }


  void PunLevelModule::_onEventSettingChanged(const char * sectionKeyValue)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, sectionKeyValue);

    char * section  = strtok((char *)buffer, kSettingsDelimiters);
    char * key      = strtok(NULL, kSettingsDelimiters);
    char * value    = strtok(NULL, kSettingsDelimiters);

    if (strcmp(section, kSettingsPunLevelSection) != 0)
    {
      return;
    }

    if (strcmp(key, kSettingsPunLevelPerPunKey) == 0)
    {
      _perPun = atoi(value);
      return;
    }

    else if (strcmp(key, kSettingsPunLevelAlarmKey) == 0)
    {
      _alarm = atoi(value);
      return;
    }

    else if (strcmp(key, kSettingsPunLevelMaxKey) == 0)
    {
      _max = atol(value);
      return;
    }

    else if (strcmp(key, kSettingsPunLevelRecoveryTimeKey) == 0)
    {
      _recoveryTime = atol(value);
      return;
    }
  }


  void PunLevelModule::_onRequestPunAdd(int8_t puns)
  {
    _addPun(puns);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  void PunLevelModule::_addPun(int8_t puns)
  {
    uint16_t newPunLevel = constrain(_punLevel + _perPun * puns, 0, _max);
    if (newPunLevel == _punLevel) return;

    _punLevel = newPunLevel;
    publish(Publication((uint8_t)Topic::PunLevel, (uint8_t)Message::EventPunLevelChanged, (uint32_t)_punLevel));

    if (_punLevel >= _alarm)
    {
      publish(Publication((uint8_t)Topic::PunLevel, (uint8_t)Message::EventPunLevelAlert, true)); // On
    }
    else
    {
      publish(Publication((uint8_t)Topic::PunLevel, (uint8_t)Message::EventPunLevelAlert, false)); // Off
    }
  }


  void PunLevelModule::_decay()
  {
    if (_punLevel == 0) return;

    _punLevel--;
    publish(Publication((uint8_t)Topic::PunLevel, (uint8_t)Message::EventPunLevelChanged, (uint32_t)_punLevel));
  }
}