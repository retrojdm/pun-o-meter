#pragma once

#include <PubSub.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  const uint8_t kSignalLightPin = 8;


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class SignalLightModule: public Publisher, public Subscriber
  {
  public:
    SignalLightModule (Broker * pBroker);
    void begin        ();
    void loop         ();
    void notify       (Publication publication);

  private:
    // Settings
    uint16_t  _duration;

    // State
    bool      _on;
    uint32_t  _lastPowered;

    // Event handlers
    void  _onEventPunLevelAlert   (bool active);
    void  _onEventSettingChanged  (const char * sectionKeyValue);

    // Private methods
    void  _setSignalLight         (bool on);
  };
}