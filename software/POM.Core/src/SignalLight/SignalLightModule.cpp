#include "SignalLightModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  SignalLightModule::SignalLightModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker)
  {
    // Settings
    _duration     = kSettingsSignalLightDuration;

    // State
    _on           = false;
    _lastPowered  = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void SignalLightModule::begin()
  {
    pinMode(kSignalLightPin, OUTPUT);
    _setSignalLight(false);

    subscribe((uint8_t)Topic::PunLevel);
    subscribe((uint8_t)Topic::Settings);
  }


  void SignalLightModule::loop()
  {
    if (!_on) return;

    uint32_t currentMillis = millis();
    if (currentMillis - _lastPowered >= (uint32_t)_duration)
    {
      _lastPowered = currentMillis;
      _setSignalLight(false);
      publish(Publication((uint8_t)Topic::SignalLight, (uint8_t)Message::EventSignalLightEnergised, false)); // Off
    }
  }


  void SignalLightModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventPunLevelAlert:   _onEventPunLevelAlert  (publication.getBool());       break;
      case Message::EventSettingChanged:  _onEventSettingChanged (publication.getCharArray());  break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event handlers

  void SignalLightModule::_onEventPunLevelAlert(bool active)
  {
    if (active)
    {
      _lastPowered = millis();
      _setSignalLight(true);
      publish(Publication((uint8_t)Topic::SignalLight, (uint8_t)Message::EventSignalLightEnergised, true)); // On
    }
    else
    {
      _setSignalLight(false);
      publish(Publication((uint8_t)Topic::SignalLight, (uint8_t)Message::EventSignalLightEnergised, false)); // Off
    }
  }


  void SignalLightModule::_onEventSettingChanged(const char * sectionKeyValue)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, sectionKeyValue);

    char * section  = strtok((char *)buffer, kSettingsDelimiters);
    char * key      = strtok(NULL, kSettingsDelimiters);
    char * value    = strtok(NULL, kSettingsDelimiters);

    if (strcmp(section, kSettingsSignalLightSection) != 0)
    {
      return;
    }

    if (strcmp(key, kSettingsSignalLightDurationKey) == 0)
    {
      _duration = atoi(value);
      return;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  void SignalLightModule::_setSignalLight(bool on)
  {
    digitalWrite(kSignalLightPin, on ? HIGH : LOW);
    _on = on;
  }
}