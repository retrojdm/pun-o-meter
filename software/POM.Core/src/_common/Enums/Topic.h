#pragma once


namespace pom
{
  enum class Topic {

    All           =   0, // 0x00 (can also be used as the topic for the serial monitor)
      
    // POM.Core
    Audio         =   1, // 0x01
    Input         =   2, // 0x02
    PunLevel      =   3, // 0x03
    Settings      =   4, // 0x04
    SignalLight   =   5, // 0x05
    Stepper       =   6, // 0x06

    // POM.Api
    Led           =   7, // 0x07
    Server        =   8, // 0x08
    Wifi          =   9  // 0x09
  };
}