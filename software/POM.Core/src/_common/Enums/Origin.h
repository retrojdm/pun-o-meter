#pragma once

namespace pom
{
  enum class Origin {
    Unknown = 0,
    POM_Core = 1,
    POM_API = 2
  };
}