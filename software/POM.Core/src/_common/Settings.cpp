#include "Settings.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  Settings::Settings()
  {
    reset();
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void Settings::reset()
  {
    // Audio
    audio.playDuration    = kSettingsAudioPlayDuration;
    audio.playCount       = kSettingsAudioPlayCount;
    memset(audio.sampleFilename, 0, kSettingsStringBuffer);
    strcpy(audio.sampleFilename, kSettingsAudioSampleFilename);

    // Pun Level
    punLevel.perPun       = kSettingsPunLevelPerPun;
    punLevel.alarm        = kSettingsPunLevelAlarm;
    punLevel.max          = kSettingsPunLevelMax;
    punLevel.recoveryTime = kSettingsPunLevelRecoveryTime;

    // Duration
    signalLight.duration  = kSettingsSignalLightDuration;

    // WiFi
    memset(wifi.ssid,     0, kSettingsStringBuffer);
    memset(wifi.password, 0, kSettingsStringBuffer);
    memset(wifi.ip,       0, kSettingsStringBuffer);
    memset(wifi.gateway,  0, kSettingsStringBuffer);
    memset(wifi.subnet,   0, kSettingsStringBuffer);
  }



  bool Settings::updateSetting(const char * section, const char * key, const char * value)
  {
    if (strcmp(section, kSettingsAudioSection) == 0)
    {
      return _updateSettingAudio(key, value);
    }

    else if (strcmp(section, kSettingsPunLevelSection) == 0)
    {
      return _updateSettingPunLevel(key, value);
    }

    else if (strcmp(section, kSettingsSignalLightSection) == 0)
    {
      return _updateSettingSignalLight(key, value);
    }

    else if (strcmp(section, kSettingsWifiSection) == 0)
    {
      return _updateSettingWiFi(key, value);
    }

    return false;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  // Audio
  bool Settings::_updateSettingAudio(const char * key, const char * value)
  {
    if (strcmp(key, kSettingsAudioPlayDurationKey) == 0)
    {
      audio.playDuration = atoi(value);
      return true;
    }

    else if (strcmp(key, kSettingsAudioPlayCountKey) == 0)
    {
      audio.playCount = atoi(value);
      return true;
    }

    else if (strcmp(key, kSettingsAudioSampleFilenameKey) == 0)
    {
      strlcpy(audio.sampleFilename, value, sizeof(audio.sampleFilename));
      return true;
    }

    return false;
  }


  // Pun level
  bool Settings::_updateSettingPunLevel(const char * key, const char * value)
  {
    if (strcmp(key, kSettingsPunLevelPerPunKey) == 0)
    {
      punLevel.perPun = atoi(value);
      return true;
    }

    else if (strcmp(key, kSettingsPunLevelAlarmKey) == 0)
    {
      punLevel.alarm = atoi(value);
      return true;
    }

    else if (strcmp(key, kSettingsPunLevelMaxKey) == 0)
    {
      punLevel.max = atoi(value);
      return true;
    }

    else if (strcmp(key, kSettingsPunLevelRecoveryTimeKey) == 0)
    {
      punLevel.recoveryTime = atoi(value);
      return true;
    }

    return false;
  }


  // Signal light
  bool Settings::_updateSettingSignalLight(const char * key, const char * value)
  {
    if (strcmp(key, kSettingsSignalLightDurationKey) == 0)
    {
      signalLight.duration = atoi(value);
      return true;
    }

    return false;
  }
  

  // WiFi
  bool Settings::_updateSettingWiFi(const char * key, const char * value)
  {
    if (strcmp(key, kSettingsWifiSsidKey) == 0)
    {
      strlcpy(wifi.ssid, value, sizeof(wifi.ssid));
      return true;
    }

    else if (strcmp(key, kSettingsWifiPasswordKey) == 0)
    {
      strlcpy(wifi.password, value, sizeof(wifi.password));
      return true;
    }

    else if (strcmp(key, kSettingsWifiIpKey) == 0)
    {
      strlcpy(wifi.ip, value, sizeof(wifi.ip));
      return true;
    }

    else if (strcmp(key, kSettingsWifiGatewayKey) == 0)
    {
      strlcpy(wifi.gateway, value, sizeof(wifi.gateway));
      return true;
    }

    else if (strcmp(key, kSettingsWifiSubnetKey) == 0)
    {
      strlcpy(wifi.subnet, value, sizeof(wifi.subnet));
      return true;
    }

    return false;
  }
}