#pragma once

namespace pom
{
  const size_t  kPublicationDataSize  = 61;
  const uint8_t kMaxPublications      = 32;
  const uint8_t kMaxSubscriptions     = 32;
}