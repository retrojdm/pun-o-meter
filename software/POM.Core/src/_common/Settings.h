#pragma once


#include <Arduino.h>

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  // Section and key names
  const char      kSettingsAudioSection[]           = "audio";
  const char      kSettingsAudioPlayDurationKey[]   = "playDuration";
  const char      kSettingsAudioPlayCountKey[]      = "playCount";
  const char      kSettingsAudioSampleFilenameKey[] = "sampleFilename";

  const char      kSettingsPunLevelSection[]        = "punLevel";
  const char      kSettingsPunLevelPerPunKey[]      = "perPun";
  const char      kSettingsPunLevelAlarmKey[]       = "alarm";
  const char      kSettingsPunLevelMaxKey[]         = "max";
  const char      kSettingsPunLevelRecoveryTimeKey[]= "recoveryTime";

  const char      kSettingsSignalLightSection[]     = "signalLight";
  const char      kSettingsSignalLightDurationKey[] = "duration";

  const char      kSettingsWifiSection[]            = "wifi";
  const char      kSettingsWifiSsidKey[]            = "ssid";
  const char      kSettingsWifiPasswordKey[]        = "password";
  const char      kSettingsWifiIpKey[]              = "ip";
  const char      kSettingsWifiGatewayKey[]         = "gateway";
  const char      kSettingsWifiSubnetKey[]          = "subnet";


  // Defaults
  const uint16_t  kSettingsStringBuffer             = 51; // max 50 chars + null terminator
  
  const uint16_t  kSettingsAudioPlayDuration        = 1500;
  const uint16_t  kSettingsAudioPlayCount           = 2;
  const char      kSettingsAudioSampleFilename[]    = "REDALERT.WAV";

  const uint16_t  kSettingsPunLevelPerPun           = 60;
  const uint16_t  kSettingsPunLevelAlarm            = 100;
  const uint16_t  kSettingsPunLevelMax              = 200;
  const uint16_t  kSettingsPunLevelRecoveryTime     = 60; // Minutes (it takes an hour to recover from a pun).

  const uint16_t  kSettingsSignalLightDuration      = 8000;


  // Publication
  const char      kSettingsDelimiter                = '\t'; // delimeter used when publishing
  const char      kSettingsDelimiters[]             = "\t"; // delimeters recognised when tokenizing  


  ////////////////////////////////////////////////////////////////////////////////
  // Structures

  struct AudioSettings
  {
    uint16_t  playDuration;
    uint16_t  playCount;
    char      sampleFilename[kSettingsStringBuffer];
  };


  struct PunLevelSettings
  {
    uint16_t  perPun;
    uint16_t  alarm;
    uint16_t  max;
    uint32_t  recoveryTime;
  };


  struct SignalLightSettings
  {
    uint16_t   duration;
  };  


  struct WifiSettings
  {
    char      ssid    [kSettingsStringBuffer];
    char      password[kSettingsStringBuffer];
    char      ip      [kSettingsStringBuffer];
    char      gateway [kSettingsStringBuffer];
    char      subnet  [kSettingsStringBuffer];
  };

  ////////////////////////////////////////////////////////////////////////////////
  // Class definitions

  class Settings
  {
  public:
    Settings                ();
    void reset              ();
    bool updateSetting      (const char * section, const char * key, const char * value);

    AudioSettings       audio;
    PunLevelSettings    punLevel;
    SignalLightSettings signalLight;
    WifiSettings        wifi;

  private:
    bool _updateSettingAudio        (const char * key, const char * value);
    bool _updateSettingPunLevel     (const char * key, const char * value);
    bool _updateSettingSignalLight  (const char * key, const char * value);
    bool _updateSettingWiFi         (const char * key, const char * value);
  };
}