#pragma once

#include <PubSub.h>
#include <SwitecX25.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants
  const uint8_t   kStepperPinB2             = 2;
  const uint8_t   kStepperPinB1             = 3;
  const uint8_t   kStepperPinA1             = 4;
  const uint8_t   kStepperPinA2             = 5;
  const uint8_t   kStepperStepsPerDegree    = 3;
  const uint16_t  kStepperMaxDegrees        = 315; // max degrees the stepper motor can turn
  const uint16_t  kStepperGaugeSweep        = 270; // degrees of sweep shown on the gauge face
  const uint16_t  kStepperGaugeRange        = 200; // max indicated value on the gauge face

  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class StepperModule : public Publisher, public Subscriber
  {
  public:
    StepperModule (Broker * pBroker);
    void begin    ();
    void loop     ();
    void notify   (Publication publication);

  private:
    void _onEventPunLevelChanged  (uint16_t punLevel);

    uint32_t  _stepWas;
    SwitecX25 _stepper;
  };
}