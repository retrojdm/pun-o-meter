#include "StepperModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  StepperModule::StepperModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker),
    _stepper(
      kStepperMaxDegrees * kStepperStepsPerDegree,
      kStepperPinA1, kStepperPinA2, kStepperPinB1, kStepperPinB2)
  {
    _stepWas = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void StepperModule::begin()
  {
    // Zero.
    _stepper.currentStep = kStepperMaxDegrees * kStepperStepsPerDegree - 1;
    _stepper.setPosition(0);

    subscribe((uint8_t)Topic::PunLevel);
  }


  void StepperModule::loop()
  {
    _stepWas = _stepper.currentStep;
    
    _stepper.update();

    if ((_stepper.currentStep == _stepper.targetStep) && (_stepWas != _stepper.currentStep))
    {
      publish(Publication(
        (uint8_t)Topic::Stepper,
        (uint8_t)Message::EventStepperTargetReached,
        (int32_t)_stepper.currentStep));
    }
  }


  void StepperModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventPunLevelChanged: _onEventPunLevelChanged (publication.getUint()); break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event handlers

  void StepperModule::_onEventPunLevelChanged(uint16_t punLevel)
  {
    int32_t pos = constrain(
      map(punLevel, 0, kStepperGaugeRange, 0, (float)kStepperGaugeSweep * kStepperStepsPerDegree),
      0,
      kStepperMaxDegrees * kStepperStepsPerDegree);

    _stepper.setPosition(pos);

    publish(Publication(
      (uint8_t)Topic::Stepper,
      (uint8_t)Message::EventStepperTargetSet,
      (int32_t)pos));
  }
}