#pragma once

namespace pom
{
  enum class AudioPlayStep {
    Idle = 0,
    Init,
    AmpPoweringUp,
    Start,
    WaitMinimumDuration,
    WaitForSampleToFinish,
    AmpPowerDown
  };
}