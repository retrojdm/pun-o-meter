#pragma once

#include <Audio.h>
#include <PubSub.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include <Wire.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"
#include "../Audio/Enums/AudioPlayStep.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  // _volume is scaled before sending to the SGTL5000.
  const uint16_t  kAudioVolumeInputMax    = 1023;
  const double    kAudioVolumeOutputMax   = 0.5;

  // SPI Pins for the SD card reader on the audio board.
  const uint8_t   kAudioSdCardPinMOSI     = 7;
  const uint8_t   kAudioSdCardPinMISO     = 12;
  const uint8_t   kAudioSdCardPinSCK      = 14;
  const uint8_t   kAudioSdCardPinCS       = 10;

  // We turn the amplifier off when we're not playing a sample to avoid noise
  // through the speakers from the WiFi.
  const uint8_t   kAudioAmpPinSW          = 20;
  const uint16_t  kAudioAmpPowerUpTime    = 250; // millis
  const uint16_t  kAudioAmpMinDuration    = 100; // millis

  const char      kAudioSamplesPath[]     = "PUN/SAMPLES/";


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class AudioModule : public Publisher, public Subscriber
  {
  public:
    AudioModule (Broker * pBroker);
    void begin  ();
    void loop   ();
    void notify (Publication publication);

  private:
    AudioPlaySdWav        _playSdWav;
    AudioOutputI2S        _i2sOut;
    AudioConnection       _patchCord1;
    AudioConnection       _patchCord2;
    AudioControlSGTL5000  _sgtl5000;

    // Settings
    uint16_t              _playDuration; // millis
    uint8_t               _playCount;
    char                  _currentSampleFilename[13]; // 8.3 format
    char                  _alertSampleFilename[13]; // 8.3 format

    // State
    uint16_t              _volume; // 0..1023
    uint32_t              _alertLastPlayed;
    uint32_t              _stepTimer;
    int16_t               _alertPlaysLeft;
    AudioPlayStep         _playStep;

    void  _onEventInputPotChanged (const char * indexValuePair);
    void  _onEventPunLevelAlert   (bool active);
    void  _onEventSettingChanged  (const char * sectionKeyValue);
    void  _onRequestAudioSamplePlay  (const char * sampleFilename);

    void  _playSample             ();
    void  _playSample             (int16_t playCount);
    void  _handlePlayCount        ();
    void  _handlePlaySteps        ();
  };
}