#include "AudioModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  AudioModule::AudioModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker),
    _playSdWav(),
    _i2sOut(),
    _patchCord1(_playSdWav, 0, _i2sOut, 0),
    _patchCord2(_playSdWav, 1, _i2sOut, 1),
    _sgtl5000()
  {
    // Settings
    _playDuration     = 1500;
    _playCount        = 2;
    memset(_currentSampleFilename, 0, kSettingsStringBuffer);
    memset(_alertSampleFilename, 0, kSettingsStringBuffer);

    // State
    _volume           = kAudioVolumeInputMax / 2;
    _alertLastPlayed  = 0;
    _stepTimer        = 0;
    _alertPlaysLeft   = 0;
    _playStep         = AudioPlayStep::Idle;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void AudioModule::begin()
  {
    AudioMemory(20);

    // Audio codec (I2S Input/Output)
    _sgtl5000.enable();
    _sgtl5000.volume(kAudioVolumeOutputMax);

    // Amp
    pinMode(kAudioAmpPinSW, OUTPUT);
    digitalWrite(kAudioAmpPinSW, LOW);

    // SD Card
    SPI.setMOSI(kAudioSdCardPinMOSI);
    SPI.setMISO(kAudioSdCardPinMISO);
    SPI.setSCK(kAudioSdCardPinSCK);
    if (!SD.begin(kAudioSdCardPinCS))
    {
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        "Card failed, or not present."));
    }

    subscribe((uint8_t)Topic::Input);
    subscribe((uint8_t)Topic::PunLevel);
    subscribe((uint8_t)Topic::Settings);
    subscribe((uint8_t)Topic::Audio);
  }


  void AudioModule::loop()
  {
    _handlePlayCount();
    _handlePlaySteps();
  }


  void AudioModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventInputPotChanged:   _onEventInputPotChanged   (publication.getCharArray()); break;
      case Message::EventPunLevelAlert:     _onEventPunLevelAlert     (publication.getBool());      break;
      case Message::EventSettingChanged:    _onEventSettingChanged    (publication.getCharArray()); break;
      case Message::RequestAudioSamplePlay: _onRequestAudioSamplePlay (publication.getCharArray()); break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event handlers

  void  AudioModule::_onEventInputPotChanged(const char * indexValuePair)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, indexValuePair);

    uint8_t index = atoi(strtok((char *)buffer, kSettingsDelimiters));
    uint16_t value = atoi(strtok(NULL, "\t"));

    if (index != 0) // pot #0 is the volume knob volume.
    {
      return;
    }

    _volume = constrain(value, 0, kAudioVolumeInputMax);
    double mappedVolume = map((double)_volume, 0.0, (double)kAudioVolumeInputMax, 0.0, kAudioVolumeOutputMax);
    _sgtl5000.volume(mappedVolume);
  }


  void AudioModule::_onEventPunLevelAlert(bool active)
  {
    if (active)
    {
      // Use the alert sample.
      strcpy(_currentSampleFilename, _alertSampleFilename);
      _playSample(_playCount);
    }
    else
    {
      _alertPlaysLeft = 0;
    }
  }


  void AudioModule::_onEventSettingChanged(const char * sectionKeyValue)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, sectionKeyValue);

    char * section  = strtok((char *)buffer, kSettingsDelimiters);
    char * key      = strtok(NULL, kSettingsDelimiters);
    char * value    = strtok(NULL, kSettingsDelimiters);

    if (strcmp(section, kSettingsAudioSection) != 0)
    {
      return;
    }
    
    if (strcmp(key, kSettingsAudioPlayDurationKey) == 0)
    {
      _playDuration = atoi(value);
      return;
    }

    else if (strcmp(key, kSettingsAudioPlayCountKey) == 0)
    {
      _playCount = atoi(value);
      return;
    }

    else if (strcmp(key, kSettingsAudioSampleFilenameKey) == 0)
    {
      strcpy(_alertSampleFilename, value);
      return;
    }
  }


  void AudioModule::_onRequestAudioSamplePlay(const char * sampleFilename)
  {
      // Use the given sample.
      strcpy(_currentSampleFilename, sampleFilename);
      _playSample(_playCount);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  void AudioModule::_playSample()
  {
    _playStep = AudioPlayStep::Init;
  }


  void AudioModule::_playSample(int16_t playCount)
  {
    _alertPlaysLeft = playCount;
    _playSample();
  }


  // If a smaple is to be played multiple times, this function handles playing
  // it at regular intervals while decrementing the play count.
  void AudioModule::_handlePlayCount()
  {
    if (_alertPlaysLeft == 0) return;

    // Next play?
    uint32_t currentMillis = millis();
    if (currentMillis - _alertLastPlayed >= _playDuration)
    {
      _alertLastPlayed = currentMillis;
      _playSample();
      _alertPlaysLeft--;
    }
  }


  // The process of playing a sample is broken into steps to allow for delays
  // without hijacking the program with delay() statements.
  // _playStep is usually zero until a sample played.
  void AudioModule::_handlePlaySteps()
  {
    switch (_playStep)
    {
    case AudioPlayStep::Init:
      digitalWrite(kAudioAmpPinSW, HIGH);
      _stepTimer = millis();
      _playStep = AudioPlayStep::AmpPoweringUp;
      break;

    case AudioPlayStep::AmpPoweringUp:
      if (millis() - _stepTimer >= kAudioAmpPowerUpTime)
      {
        _playStep = AudioPlayStep::Start;
      }
      break;

    case AudioPlayStep::Start:
      char filepath[kSettingsStringBuffer];
      strcpy(filepath, kAudioSamplesPath);
      strcat(filepath, _currentSampleFilename);

      publish(Publication(
        (uint8_t)Topic::Audio,
        (uint8_t)Message::EventAudioSamplePlaying,
        filepath));

      _playSdWav.play(filepath);
      _stepTimer = millis();
      _playStep = AudioPlayStep::WaitMinimumDuration;
      break;

    case AudioPlayStep::WaitMinimumDuration:
      if (millis() - _stepTimer >= kAudioAmpMinDuration)
      {
        _playStep = AudioPlayStep::WaitForSampleToFinish;
      }
      break;

    case AudioPlayStep::WaitForSampleToFinish:
      if (!_playSdWav.isPlaying())
      {
        _playStep = AudioPlayStep::AmpPowerDown;
        publish(Publication(
          (uint8_t)Topic::Audio,
          (uint8_t)Message::EventAudioSampleStopped));
      }
      break;

    case AudioPlayStep::AmpPowerDown:
      digitalWrite(kAudioAmpPinSW, LOW);      
      _playStep = AudioPlayStep::Idle;
      break;
    }
  }
}