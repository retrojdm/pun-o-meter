#pragma once

#include <ArduinoJson.h>
#include <Audio.h>
#include <PubSub.h>
#include <SD.h>
#include <SPI.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  const char      kSettingsModuleSettingsFilepath[]     = "PUN/SETTIN~1.JSO"; // pun\settings.json
  const char      kSettingsModuleWifiSettingsFilepath[] = "PUN/WIFI~1.JSO";   // pun\wifi.json
  const char      kSettingsModuleSamplesPath[]          = "PUN/SAMPLES/";
  const uint8_t   kSettingsModuleSdCardPinSS            = 10;   // Audio shield has SD Card chip select on pin 10
  const uint8_t   kSettingsModuleSpiPinMosi             = 7;    // Audio shield has MOSI on pin 7
  const uint8_t   kSettingsModuleSpiPinSck              = 14;   // Audio shield has SCK on pin 14
  const uint16_t  kSettingsModuleJsonBuffer             = 512;


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class SettingsModule: public Publisher, public Subscriber
  {
  public:
    SettingsModule  (Broker * pBroker);
    void begin      ();
    void loop       ();
    void notify     (Publication publication);    

  private:
    Settings      _settings;
    bool          _samplesPublished;
    File          _samplesDir;  // to avoid filling the publications array,
                                // we step through the samples dir in the loop.
                                // This way we only generate one publication per loop.

    // Event handlers
    void  _onRequestSettingSet      (const char * sectionKeyValue);
    void  _onRequestSettingsSave    ();
    void  _onRequestSettingsReset   ();
    
    // Private methods

    // SD Card
    void _loadSettings              ();
    void _saveSettings              ();
    void _writeJsonToFile           (JsonObject & root, const char * filepath);
    void _loadWifiSettings          ();

    // JSON
    void _parseSettings             (File & file);
    void _parseWifiSettings         (File & file);
    
    // Publishing
    void _publishSettings           ();
    void _publishWifiSettings       ();
    void _publishSetting            (const char * section, const char * key, uint32_t value);
    void _publishSetting            (const char * section, const char * key, const char * value);

    // Samples
    void _startPublishingSamples    ();
    void _publishNextSample         ();
  };
}