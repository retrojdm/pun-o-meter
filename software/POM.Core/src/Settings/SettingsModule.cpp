#include "SettingsModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  SettingsModule::SettingsModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker),
    _settings()
  {
    _samplesPublished = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void SettingsModule::begin()
  {
    // Main settings
    _loadSettings();

    // WiFi settings (read only)
    // In a separate JSON file on the SD card.
    _loadWifiSettings();

    // Listen for requests
    subscribe((uint8_t)Topic::Settings);

    // publish settings.
    _publishSettings();
    _publishWifiSettings();
    _startPublishingSamples();    
  }


  void SettingsModule::loop()
  {
    if (!_samplesPublished)
    {
      _publishNextSample();
    }
  }


  void SettingsModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::RequestSettingSet:      _onRequestSettingSet      (publication.getCharArray()); break;
      case Message::RequestSettingsSave:    _onRequestSettingsSave    ();                           break;
      case Message::RequestSettingsReset:   _onRequestSettingsReset   ();                           break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event Handlers

  // Event messages aren't very long.
  // We can't fit the entire Settings object as raw data, so we publish the
  // individual settings one at a time.
  void SettingsModule::_onRequestSettingSet(const char * sectionKeyValue)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, sectionKeyValue);

    char * section  = strtok((char *)buffer, kSettingsDelimiters);
    char * key      = strtok(NULL, kSettingsDelimiters);
    char * value    = strtok(NULL, kSettingsDelimiters);

    if (_settings.updateSetting(section, key, value))
    {
      _publishSetting(section, key, value);
    }
  }

  // Since settings are transmitted one at a time, we use a separate "Save"
  // request to be called at the end.
  // This saves us from writing to the SD car after each individual setting is
  // published.
  void SettingsModule::_onRequestSettingsSave()
  {
    _saveSettings();
    _publishSettings();
  }  


  // Resets main settings to defaults.
  void SettingsModule::_onRequestSettingsReset()
  {
    _settings.reset();
    _saveSettings();
    _publishSettings();
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods - SD Card

  // Load settings from a JSON file on the SD card.
  void SettingsModule::_loadSettings()
  {
    AudioNoInterrupts();

    // We assume the SD card has already been initialised
    // in the Audio module.
    File file = SD.open(kSettingsModuleSettingsFilepath);
    if (file)
    {
      _parseSettings(file);
      file.close();
    }  
    else
    {
      char error[81];
      sprintf(error, "Error opening \"%s\"", kSettingsModuleSettingsFilepath);
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        error));
    } 

    AudioInterrupts();
  }


  // Write _only_ the main settings to a JSON file on the SD card.
  // (not the WiFi settings)
  void SettingsModule::_saveSettings()
  {
    StaticJsonBuffer<kSettingsModuleJsonBuffer> jsonBuffer;
    
    JsonObject & root         = jsonBuffer.createObject();
    JsonObject & audio        = jsonBuffer.createObject();
    JsonObject & punLevel     = jsonBuffer.createObject();
    JsonObject & signalLight  = jsonBuffer.createObject();

    // Audio
    audio[kSettingsAudioPlayDurationKey]        = _settings.audio.playDuration;
    audio[kSettingsAudioPlayCountKey]           = _settings.audio.playCount;
    audio[kSettingsAudioSampleFilenameKey]      = _settings.audio.sampleFilename;
    root[kSettingsAudioSection] = audio;

    // Pun level
    punLevel[kSettingsPunLevelPerPunKey]        = _settings.punLevel.perPun;
    punLevel[kSettingsPunLevelAlarmKey]         = _settings.punLevel.alarm;
    punLevel[kSettingsPunLevelMaxKey]           = _settings.punLevel.max;
    punLevel[kSettingsPunLevelRecoveryTimeKey]  = _settings.punLevel.recoveryTime;
    root[kSettingsPunLevelSection] = punLevel;

    // Signal light
    signalLight[kSettingsSignalLightDurationKey] = _settings.signalLight.duration;
    root[kSettingsSignalLightSection] = signalLight;

    _writeJsonToFile(root, kSettingsModuleSettingsFilepath);
  }


  // Writes a JSON object to the specified file on the SD card.
  void SettingsModule::_writeJsonToFile(JsonObject & root, const char * filepath)
  {
    char textBuffer[kSettingsModuleJsonBuffer];
    root.prettyPrintTo(textBuffer);

    AudioNoInterrupts();

    File file = SD.open(filepath, O_WRITE | O_CREAT | O_TRUNC); // Overwrite file if it exists.
    if (file)
    {
      file.print(textBuffer);
      file.close();
    }
    else
    {
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        "Error writing settings to SD card."));
    }

    AudioInterrupts();
  }

  
  // Load WiFi settings from a JSON file on the SD card.
  void SettingsModule::_loadWifiSettings()
  {
    AudioNoInterrupts();

    // We assume the SD card has already been initialised
    // in the Audio module.
    File file = SD.open(kSettingsModuleWifiSettingsFilepath);
    if (file)
    {
      _parseWifiSettings(file);
      file.close();
    }  
    else
    {
      char error[81];
      sprintf(error, "Error opening \"%s\"", kSettingsModuleWifiSettingsFilepath);
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        error));
    } 

    AudioInterrupts();
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods - Parse JSON

  // Try to parse the contents of the settings json file on the SD card.
  // If successfully parsed, the settings are copied into the _settings object.
  void SettingsModule::_parseSettings(File & file)
  {
    // Try to parse file as JSON.
    StaticJsonBuffer<kSettingsModuleJsonBuffer> jsonBuffer;
    JsonObject & root = jsonBuffer.parseObject(file);

    // No good?
    if (!root.success())
    {
      char error[kPublicationDataSize];
      sprintf(error, "Error parsing \"%s\"", kSettingsModuleSettingsFilepath);
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        error));
      return;
    }

    // Copy values from the JsonObject to the _settings object.

    // Audio
    JsonObject &audio = root[kSettingsAudioSection];
    _settings.audio.playDuration    = audio[kSettingsAudioPlayDurationKey]          | kSettingsAudioPlayDuration;
    _settings.audio.playCount       = audio[kSettingsAudioPlayCountKey]             | kSettingsAudioPlayCount;
    strlcpy(_settings.audio.sampleFilename, audio[kSettingsAudioSampleFilenameKey]  | kSettingsAudioSampleFilename,  sizeof(_settings.audio.sampleFilename));

    // Pun level
    JsonObject &punLevel = root[kSettingsPunLevelSection];
    _settings.punLevel.perPun       = punLevel[kSettingsPunLevelPerPunKey]          | kSettingsPunLevelPerPun;
    _settings.punLevel.alarm        = punLevel[kSettingsPunLevelAlarmKey]           | kSettingsPunLevelAlarm;
    _settings.punLevel.max          = punLevel[kSettingsPunLevelMaxKey]             | kSettingsPunLevelMax;
    _settings.punLevel.recoveryTime = punLevel[kSettingsPunLevelRecoveryTimeKey]    | kSettingsPunLevelRecoveryTime;

    // Signal light
    JsonObject &signalLight = root[kSettingsSignalLightSection];
    _settings.signalLight.duration  = signalLight[kSettingsSignalLightDurationKey]  | kSettingsSignalLightDuration;
  }


  // Try to parse the contents of the wifi json file on the SD card.
  // If successfully parsed, the wifi settings are copied into the _settings
  // object.
  void SettingsModule::_parseWifiSettings(File & file)
  {
    // Try to parse file as JSON.
    StaticJsonBuffer<kSettingsModuleJsonBuffer> jsonBuffer;
    JsonObject &root = jsonBuffer.parseObject(file);

    // No good?
    if (!root.success())
    {
      char error[kPublicationDataSize];
      sprintf(error, "Error parsing \"%s\"", kSettingsModuleWifiSettingsFilepath);
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        error));
      return;
    }

    // Copy values from the JsonObject to the _wifiSettings object.
    strlcpy(_settings.wifi.ssid,     root[kSettingsWifiSsidKey]      | "",  sizeof(_settings.wifi.ssid));
    strlcpy(_settings.wifi.password, root[kSettingsWifiPasswordKey]  | "",  sizeof(_settings.wifi.password));
    strlcpy(_settings.wifi.ip,       root[kSettingsWifiIpKey]        | "",  sizeof(_settings.wifi.ip));
    strlcpy(_settings.wifi.gateway,  root[kSettingsWifiGatewayKey]   | "",  sizeof(_settings.wifi.gateway));
    strlcpy(_settings.wifi.subnet,   root[kSettingsWifiSubnetKey]    | "",  sizeof(_settings.wifi.subnet));
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods - Publishing

  // Publish only the main settings. Not the WiFi settings.
  void SettingsModule::_publishSettings()
  {
    _publishSetting(kSettingsAudioSection,        kSettingsAudioPlayDurationKey,    _settings.audio.playDuration);
    _publishSetting(kSettingsAudioSection,        kSettingsAudioPlayCountKey,       _settings.audio.playCount);
    _publishSetting(kSettingsAudioSection,        kSettingsAudioSampleFilenameKey,  _settings.audio.sampleFilename);
    _publishSetting(kSettingsPunLevelSection,     kSettingsPunLevelPerPunKey,       _settings.punLevel.perPun);
    _publishSetting(kSettingsPunLevelSection,     kSettingsPunLevelAlarmKey,        _settings.punLevel.alarm);
    _publishSetting(kSettingsPunLevelSection,     kSettingsPunLevelMaxKey,          _settings.punLevel.max);
    _publishSetting(kSettingsPunLevelSection,     kSettingsPunLevelRecoveryTimeKey, _settings.punLevel.recoveryTime);
    _publishSetting(kSettingsSignalLightSection,  kSettingsSignalLightDurationKey,  _settings.signalLight.duration);
  }

 
 // Publish the WiFi settings.
  void SettingsModule::_publishWifiSettings()
  {
    _publishSetting(kSettingsWifiSection, kSettingsWifiSsidKey,     _settings.wifi.ssid);
    _publishSetting(kSettingsWifiSection, kSettingsWifiPasswordKey, _settings.wifi.password);
    _publishSetting(kSettingsWifiSection, kSettingsWifiIpKey,       _settings.wifi.ip);
    _publishSetting(kSettingsWifiSection, kSettingsWifiGatewayKey,  _settings.wifi.gateway);
    _publishSetting(kSettingsWifiSection, kSettingsWifiSubnetKey,   _settings.wifi.subnet);
  }


  // Publish an individual setting as an unsigned long.  
  void SettingsModule::_publishSetting(const char * section, const char * key, uint32_t value)
  {
    char sectionKeyValue[kPublicationDataSize];
    sprintf(sectionKeyValue, "%s%c%s%c%lu", section, kSettingsDelimiter, key, kSettingsDelimiter, value);
    publish(Publication((uint8_t)Topic::Settings, (uint8_t)Message::EventSettingChanged, sectionKeyValue));
  }


  // Publish an individual setting as a string (char array). 
  void SettingsModule::_publishSetting(const char * section, const char * key, const char * value)
  {
    char sectionKeyValue[kPublicationDataSize];
    sprintf(sectionKeyValue, "%s%c%s%c%s", section, kSettingsDelimiter, key, kSettingsDelimiter, value);
    publish(Publication((uint8_t)Topic::Settings, (uint8_t)Message::EventSettingChanged, sectionKeyValue));
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods - Samples

  void SettingsModule::_startPublishingSamples()
  {
    AudioNoInterrupts();    

    // We assume the SD card has already been initialised
    // in the Audio module.
    _samplesDir = SD.open(kSettingsModuleSamplesPath);
    if (_samplesDir)
    {
      // Other than publish a 'cleared' event, we don't do anything here.
      //
      // We intentionally leave the dir object 'open'.
      // Processing is done in _publishNextSample(), which is called in the
      // .loop() (to avoid publishing too many events at once).
      //
      publish(Publication(
        (uint8_t)Topic::Settings,
        (uint8_t)Message::EventSettingsSamplesCleared));
    }
    else
    {
      char error[81];
      sprintf(error, "Path not found: \"%s\"", kSettingsModuleSamplesPath);
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        error));
    } 
  }


  void SettingsModule::_publishNextSample()
  {
    File entry = _samplesDir.openNextFile();
    if (!entry)
    {
      _samplesDir.close();
      _samplesPublished = true;
      AudioInterrupts();
      return;
    }

    if (!(entry.isDirectory()))
    {
      // We list all files in the samples dir (even if they're not WAV files).
      publish(Publication(
        (uint8_t)Topic::Settings,
        (uint8_t)Message::EventSettingsSampleAdded,
        entry.name()));
    }
  }
}