////////////////////////////////////////////////////////////////////////////////
// PUN-O-METER - API
// Andrew Wyatt - retrojdm.com


////////////////////////////////////////////////////////////////////////////////
// Libraries

#include <Arduino.h>

// Ring Buffer (FIFO / Queue object) used by thr PubSub library
// https://bitbucket.org/retrojdm/ringbuffer
#include <RingBuffer.h>

// Arduino Publish/Subscriber design pattern
// https://bitbucket.org/retrojdm/pub-sub
#include <PubSub.h>

// Server module
// https://github.com/bblanchon/ArduinoJson/
//
// Note:  We're using version 5.13.2 of ArduinoJson
//        There were breaking changes in version 6.
//        eg: JsonBuffer was renamed JsonDocument
//
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>

// Wifi module
#include <ESP8266WiFi.h>
#include <WiFiClient.h>


////////////////////////////////////////////////////////////////////////////////
// Main program

#include "src/_common/Enums/Origin.h"
#include "src/_common/Constants.h"
#include "src/Led/LedModule.h"
#include "src/Serial/SerialModule.h"
#include "src/Server/ServerModule.h"
#include "src/Wifi/WifiModule.h"


using namespace pom;


// This Broker object will take care of the global event bus and message queue.
Broker gBroker(kPublicationDataSize, kMaxPublications, kMaxSubscriptions);

LedModule     gLed    (&gBroker);
SerialModule  gSerial (&gBroker);
ServerModule  gServer (&gBroker);
WifiModule    gWifi   (&gBroker);


void setup()
{
  gBroker.begin();

  gLed.begin();
  gSerial.begin();
  gServer.begin();
  gWifi.begin();
}


void loop()
{
  gBroker.distribute();
  
  gLed.loop();
  gSerial.loop();
  gServer.loop();
  gWifi.loop();
}
