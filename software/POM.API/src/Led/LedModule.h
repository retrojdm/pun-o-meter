#pragma once

#include <Arduino.h>
#include <PubSub.h>
#include <ESP8266WiFi.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"


namespace pom
{

  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  const uint8_t   kLedPin                   = 16;   // GPIO16 == Pin 4 == D0
  const uint8_t   kLedFlashInterval         = 200;  // millis between status change (eg: on => off)

  const uint16_t  kLedBitSequenceOff        = 0b0000000000000000;
  const uint16_t  kLedBitSequenceOn         = 0b1111111111111111;
  const uint16_t  kLedBitSequenceFlashing   = 0b1100110011001100;
  const uint16_t  kLedBitSequenceDotX1      = 0b0000000000000001;
  const uint16_t  kLedBitSequenceDotX2      = 0b0000000000000101;
  const uint16_t  kLedBitSequenceDotX3      = 0b0000000000010101;
  const uint16_t  kLedBitSequenceDotX4      = 0b0000000001010101;
  const uint16_t  kLedBitSequenceDotX5      = 0b0000000101010101;
  const uint16_t  kLedBitSequenceDotX6      = 0b0000010101010101;
  const uint16_t  kLedBitSequenceDotX7      = 0b0001010101010101;
  const uint16_t  kLedBitSequenceDotDashDot = 0b0000000001011101;


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class LedModule : public Publisher, public Subscriber
  {
  public:
    LedModule   (Broker * pBroker);
    void begin  ();
    void loop   ();
    void notify (Publication publication);

  private:
    uint8_t   _index;
    uint16_t  _bitSequence;
    uint32_t  _lastChanged;

    void      _onEventWifiConnectionStatusChanged (uint8_t status);

    void      _update         ();
    void      _setBitSequence (uint16_t bitSequence);    
  };
}