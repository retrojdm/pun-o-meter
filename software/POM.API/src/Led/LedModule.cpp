#include "LedModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  LedModule::LedModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker)
  {
    _index        = 0;
    _bitSequence  = kLedBitSequenceFlashing;
    _lastChanged  = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public functions

  void LedModule::begin()
  {
    pinMode(kLedPin, OUTPUT);
    _update();

    Publication pub(
      (uint8_t)Topic::Led,
      (uint8_t)Message::EventLedSequenceChanged,
      kPublicationDataTypeBitSequence,
      sizeof(_bitSequence),
      _bitSequence);
    publish(pub);

    subscribe((uint8_t)Topic::Settings);
    subscribe((uint8_t)Topic::Wifi);
  }


  void LedModule::loop()
  {
    unsigned long currentMillis = millis();
    if (currentMillis - _lastChanged >= kLedFlashInterval)
    {
      _lastChanged = currentMillis;
      _index++;
      if (_index >= 16) 
      {
        _index = 0;
      }
      _update();
    }
  }


  void LedModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventWifiConnectionStatusChanged: _onEventWifiConnectionStatusChanged(publication.getUint()); break;
    }
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Private functions

  void LedModule::_onEventWifiConnectionStatusChanged(uint8_t status)
  {
    uint16_t bitSequence;

    // See wl_status_t
    // C:\Program files (x86)\Arduino\libraries\WiFi\src\utility\wl_definitions.h
    switch (status)
    {
      case WL_NO_SHIELD:        bitSequence = kLedBitSequenceDotDashDot;  break;
      case WL_IDLE_STATUS:      bitSequence = kLedBitSequenceFlashing;    break;
      case WL_NO_SSID_AVAIL:    bitSequence = kLedBitSequenceDotX1;       break;
      case WL_SCAN_COMPLETED:   bitSequence = kLedBitSequenceDotX2;       break;
      case WL_CONNECTED:        bitSequence = kLedBitSequenceOn;          break;
      case WL_CONNECT_FAILED:   bitSequence = kLedBitSequenceDotX4;       break;
      case WL_CONNECTION_LOST:  bitSequence = kLedBitSequenceDotX5;       break;
      case WL_DISCONNECTED:     bitSequence = kLedBitSequenceDotX6;       break;
      default:                  bitSequence = kLedBitSequenceDotX7;       break;
    }

    _setBitSequence(bitSequence);
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private functions

  void LedModule::_setBitSequence(uint16_t bitSequence)
  {
    _bitSequence = bitSequence;
    _index = 0;
    _lastChanged = millis();
    _update();

    Publication pub(
      (uint8_t)Topic::Led,
      (uint8_t)Message::EventLedSequenceChanged,
      _bitSequence);
    pub.dataType = '0'; // '0' = bit sequence
    publish(pub);
  }


  void LedModule::_update()
  {
    uint16_t bit = (_bitSequence >> _index) & 1;
    digitalWrite(kLedPin, bit == 1 ? LOW : HIGH); // LOW = on.
  }
}