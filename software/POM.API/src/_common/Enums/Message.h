#pragma once


namespace pom
{
  enum class Message {

    None                              = 0x00,

    ////////////////////////////////////////////////////////////////////////////////
    // Main board

    // Serial monitor    
    RequestSerialMonitorDebugMessage  = 0x01, // char *   message

    // Audio
    EventAudioSamplePlaying           = 0x10, // char *   filename
    EventAudioSampleStopped           = 0x11, //
    RequestAudioSamplePlay            = 0x12, // char *   filename

    // Input
    EventInputButtonPressed           = 0x20, // uint8_t  button index
    EventInputPotChanged              = 0x21, // char *   indexValuePair

    // PunLevel
    EventPunLevelChanged              = 0x30, // uint16_t punLevel
    EventPunLevelAlert                = 0x31, // bool     active
    RequestPunAdd                     = 0x32, // uint8_t  puns

    // Settings
    RequestSettingSet                 = 0x40, // char *   keyValuePair
    EventSettingChanged               = 0x41, // char *   keyValuePair
    RequestSettingsSave               = 0x42, //
    RequestSettingsReset              = 0x43, //
    EventSettingsSamplesCleared       = 0x44, //
    EventSettingsSampleAdded          = 0x45, // char *   filename

    // SignalLight
    EventSignalLightEnergised         = 0x50, // bool     energised

    // Stepper
    EventStepperTargetSet             = 0x60, // uint16_t position
    EventStepperTargetReached         = 0x61, // uint16_t position
                                                                              

    ////////////////////////////////////////////////////////////////////////////////
    // Front panel

    // Led
    EventLedSequenceChanged           = 0x70, // uint16_t status

    // Server
    EventServerRunning                = 0x80, // 
    EventServerRequest                = 0x81, // char *   resource
    EventServerResponse               = 0x82, // uint16_t code

    // Wifi
    EventWifiConnectionStatusChanged  = 0x90, // uint8_t  status
    EventWifiConnectionAttempt        = 0x91  // char *   ssid
  };
}