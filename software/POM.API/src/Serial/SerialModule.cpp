#include "SerialModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  SerialModule::SerialModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker)
  {
    memset(_inputBuffer, 0, kSerialSerialisedPacketMaxSize);
    _pInputBuffer = _inputBuffer;
    _inputLength  = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void SerialModule::begin()
  {   
    Serial.begin(kSerialBaudRate);
    delay(100);

    subscribe((uint8_t)Topic::All);
  }


  void SerialModule::loop()
  {
    while (Serial.available())
    {
      char c = Serial.read();
      if (c == '\n') // packets are new-line separated.
      {
        _onPacketReceived(_inputBuffer);
        _pInputBuffer = _inputBuffer; // reset.
        *_pInputBuffer = '\0';
        _inputLength = 0;
        return;
      }

      // Ignore carriage return char. Stop reading if buffer is full.
      else if ((c != '\r') && (_inputLength < kSerialSerialisedPacketMaxSize - 1))
      {
        _inputLength++;
        *(_pInputBuffer++) = c;
        *_pInputBuffer = '\0';
      }

      yield();
    }
  }


  // Transmit the messages to the POM.API board.
  void SerialModule::notify(Publication publication)
  {
    if ((publication.origin == 0) || (publication.origin == kSerialOrigin))
    {
      publication.origin = kSerialOrigin;
      Packet packet(publication);
      char buffer[kSerialSerialisedPacketMaxSize];
      packet.serialise(buffer);
      Serial.println(buffer);
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  void SerialModule::_onPacketReceived(char * serialised)
  {
    uint8_t buffer[kPublicationDataSize];
    Packet packet(serialised, buffer);
    publish(packet.getPublication());
  }
}