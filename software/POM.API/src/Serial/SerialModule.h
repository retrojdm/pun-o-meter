#pragma once

#include <Arduino.h>
#include <PubSub.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Enums/Origin.h"
#include "../_common/Constants.h"


namespace pom
{
  // The baud rates must match for the Serial module (not SerialMonitor) on each
  // board.
  //
  // Note:  The ESP8266 module's ROM outputs a startup message at 74880 baud.
  //        
  //        Using a different rate seems to be enough to ignore it. although, this
  //        could possibly be POM.API's call to Serial.begin() clearing the output
  //        buffer before POM.Core has chance to start.
  //
  const uint8_t   kSerialOrigin                   = (uint8_t)Origin::POM_API;
  const uint16_t  kSerialBaudRate                 = 38400;
  const uint8_t   kSerialSerialisedPacketMaxSize  = 128;


  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  class SerialModule : public Publisher, public Subscriber
  {
    public:
      SerialModule          (Broker * pBroker);
      void begin            ();
      void loop             ();
      void notify           (Publication publication);

     private:
      void _onPacketReceived  (char * serialised);
      
      char     _inputBuffer[kSerialSerialisedPacketMaxSize];
      char *   _pInputBuffer;
      uint8_t  _inputLength;
  };
}