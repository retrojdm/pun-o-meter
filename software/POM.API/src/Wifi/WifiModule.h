#pragma once

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSub.h>
#include <WiFiClient.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class WifiModule : public Publisher, public Subscriber
  {
  public:
    WifiModule  (Broker * pBroker);
    void begin  ();
    void loop   ();
    void notify (Publication publication);

  private:
    wl_status_t     _status;
    char            _ssid     [kPublicationDataSize];
    char            _password [kPublicationDataSize];
    char            _ip       [kPublicationDataSize];
    char            _gateway  [kPublicationDataSize];
    char            _subnet   [kPublicationDataSize];

    void _onEventSettingChanged (const char * sectionKeyValue);

    void _handleConnection      ();
  };
}