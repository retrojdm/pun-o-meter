#include "WifiModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  WifiModule::WifiModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker)
  {
    _status       = WL_IDLE_STATUS;

    _ssid[0]      = '\0';
    _password[0]  = '\0';
    _ip[0]        = '\0';
    _gateway[0]   = '\0';
    _subnet[0]    = '\0';

    /*
    strcpy(_ssid,     "...");
    strcpy(_password, "...");
    strcpy(_ip,       "192.168.0.201");
    strcpy(_gateway,  "192.168.0.1");
    strcpy(_subnet,   "255.255.255.0");
    _handleConnection();
    */
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public functions

  void WifiModule::begin()
  {
    WiFi.mode(WIFI_STA);

    subscribe((uint8_t)Topic::Settings);
  }


  void WifiModule::loop()
  {
    wl_status_t statusWas = _status;
    _status = WiFi.status();

    // Publish an event if the status has changed.
    if (_status != statusWas)
    {
      publish(Publication((uint8_t)Topic::Wifi, (uint8_t)Message::EventWifiConnectionStatusChanged, _status));
    }
  }


  void WifiModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventSettingChanged:  _onEventSettingChanged  (publication.getCharArray()); break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event handlers

  void WifiModule::_onEventSettingChanged(const char * sectionKeyValue)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, sectionKeyValue);

    char * section  = strtok((char *)buffer, kSettingsDelimiters);
    char * key      = strtok(NULL, kSettingsDelimiters);
    char * value    = strtok(NULL, kSettingsDelimiters);

    if (strcmp(section, kSettingsWifiSection) != 0)
    {
      return;
    }

    if (strcmp(key, kSettingsWifiSsidKey) == 0)
    {
      strcpy(_ssid, value);
    }

    else if (strcmp(key, kSettingsWifiPasswordKey) == 0)
    {
      strcpy(_password, value);
    }

    else if (strcmp(key, kSettingsWifiIpKey) == 0)
    {
      strcpy(_ip, value);
    }   

    else if (strcmp(key, kSettingsWifiGatewayKey) == 0)
    {
      strcpy(_gateway, value);
    } 

    else if (strcmp(key, kSettingsWifiSubnetKey) == 0)
    {
      strcpy(_subnet, value);
    }

    _handleConnection();
  }

  
  ////////////////////////////////////////////////////////////////////////////////
  // Private methods

  void WifiModule::_handleConnection()
  {
    // Don't attempt to connect if we're already connected or any setting is blank.
    if (
      (_status == WL_CONNECTED) ||
      (strlen(_ssid)      == 0) ||
      (strlen(_password)  == 0) ||
      (strlen(_ip)        == 0) ||
      (strlen(_gateway)   == 0) ||
      (strlen(_subnet)    == 0))
    {
      return;
    }

    // Convert from strings.
    IPAddress ip;
    IPAddress gateway;
    IPAddress subnet;      
    ip.fromString(_ip);
    gateway.fromString(_gateway);
    subnet.fromString(_subnet);

    // Try to connect.
    WiFi.config(ip, gateway, subnet);
    WiFi.begin(_ssid, _password);        

    // Generate an 'attempt' event.
    publish(Publication(
      (uint8_t)Topic::Wifi,
      (uint8_t)Message::EventWifiConnectionAttempt,
      _ssid));
  }
}