#include "ServerModule.h"

namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Class constructor

  ServerModule::ServerModule(Broker * pBroker) :
    Publisher(pBroker),
    Subscriber(pBroker),
    _server(kServerPort)
  {
    _running = false;

    _punLevel = 0;
    _sampleCount = 0;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Public methods

  void ServerModule::begin()
  {
    _server.onNotFound  (                              std::bind(&ServerModule::_notFound,      this));
    _server.on          ("/",               HTTP_GET,  std::bind(&ServerModule::_root,          this));
    _server.on          ("/addpun",         HTTP_POST, std::bind(&ServerModule::_addPun,        this));
    _server.on          ("/getpunlevel",    HTTP_GET,  std::bind(&ServerModule::_getPunLevel,   this));
    _server.on          ("/getsettings",    HTTP_GET,  std::bind(&ServerModule::_getSettings,   this));
    _server.on          ("/setsettings",    HTTP_POST, std::bind(&ServerModule::_setSettings,   this));
    _server.on          ("/resetsettings",  HTTP_POST, std::bind(&ServerModule::_resetSettings, this));
    _server.on          ("/listsamples",    HTTP_GET,  std::bind(&ServerModule::_listSamples,   this));
    _server.on          ("/playsample",     HTTP_POST, std::bind(&ServerModule::_playSample,    this));

    _punLevel = 0;
    _settings.reset();
    _running = false;

    subscribe((uint8_t)Topic::PunLevel);
    subscribe((uint8_t)Topic::Settings);
    subscribe((uint8_t)Topic::Wifi);
  }


  void ServerModule::loop()
  {
    if (_running)
    {
      _server.handleClient();
    }
  }


  void ServerModule::notify(Publication publication)
  {
    switch ((Message)publication.message)
    {
      case Message::EventWifiConnectionStatusChanged: _onEventWifiConnectionStatusChanged (publication.getUint());      break;
      case Message::EventPunLevelChanged:             _onEventPunLevelChanged             (publication.getUint());      break;
      case Message::EventSettingChanged:              _onEventSettingChanged              (publication.getCharArray()); break;
      case Message::EventSettingsSamplesCleared:      _onEventSettingsSamplesCleared      ();                           break;
      case Message::EventSettingsSampleAdded:         _onEventSettingsSampleAdded         (publication.getCharArray()); break;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Event handlers

  void ServerModule::_onEventWifiConnectionStatusChanged(uint8_t status)
  {
    if ((status == WL_CONNECTED) && !_running)
    {
      _server.begin();     
      _running = true;

      char localIP[16];
      WiFi.localIP().toString().toCharArray(localIP, 16);

      publish(Publication(
        (uint8_t)Topic::Server,
        (uint8_t)Message::EventServerRunning,
        localIP));
    }
  }


  void ServerModule::_onEventPunLevelChanged(uint16_t punLevel)
  {
    _punLevel = punLevel;
  }


  void ServerModule::_onEventSettingChanged(const char * sectionKeyValue)
  {
    char buffer[kPublicationDataSize];
    strcpy(buffer, sectionKeyValue);

    char * section  = strtok((char *)buffer, kSettingsDelimiters);
    char * key      = strtok(NULL, kSettingsDelimiters);
    char * value    = strtok(NULL, kSettingsDelimiters);

    // Update the server module's local copy of the settings.
    _settings.updateSetting(section, key, value);
  }


  void ServerModule::_onEventSettingsSamplesCleared()
  {
    _sampleCount = 0;
  }


  void ServerModule::_onEventSettingsSampleAdded(const char * filename)
  {
    if (_sampleCount < kServerSamplesListMax)
    {
      strlcpy(_samples[_sampleCount], filename, 13);
      _sampleCount++;
    }
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Server helpers

  void ServerModule::_sendResponse(uint16_t code)
  {
    _server.send(code, "text/plain", "");
    _publishResponse(code);
  }


  void ServerModule::_sendResponse(uint16_t code, const char * message)
  {
    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    char bodyText[kServerTextBufferLength];

    JsonObject & response = jsonBuffer.createObject();
    response["message"] = message;
    response.printTo(bodyText, kServerTextBufferLength);
    _server.send(code, "application/json", bodyText);
    _publishResponse(code);
  }


  void ServerModule::_sendResponse(uint16_t code, JsonObject &response)
  {
    char bodyText[kServerTextBufferLength];
    response.printTo(bodyText, kServerTextBufferLength);
    _server.send(code, "application/json", bodyText);
    _publishResponse(code);
  }


  void ServerModule::_sendResponse(uint16_t code, JsonArray &response)
  {
    char bodyText[kServerTextBufferLength];
    response.printTo(bodyText, kServerTextBufferLength);
    _server.send(code, "application/json", bodyText);
    _publishResponse(code);
  }


  // Sets the '_settings' setting only if it's in the JSON collection.
  // Publishes a 'RequestSettingSet' request if it is.
  void ServerModule::_setSetting(
    JsonObject & json, const char * section, const char * key)
  {
    if (!json.containsKey(key)) 
    {
      char error[kPublicationDataSize];
      sprintf(error, "\"%s\" not found in \"%s\".", key, section);
      publish(Publication(
        (uint8_t)Topic::All,
        (uint8_t)Message::RequestSerialMonitorDebugMessage,
        error));
      return;
    }

    const char * value = json[key];
    char sectionKeyValue[kPublicationDataSize];
    sprintf(sectionKeyValue, "%s%c%s%c%s",
      section, kSettingsDelimiter,
      key, kSettingsDelimiter,
      value);

    _settings.updateSetting(section, key, value);

    publish(Publication(
      (uint8_t)Topic::Settings,
      (uint8_t)Message::RequestSettingSet,
      sectionKeyValue));
  }


  // Publish the resource being requested by the client.
  void ServerModule::_publishRequest()
  {
    char resource[kPublicationDataSize];
    _server.uri().toCharArray(resource, kPublicationDataSize);
    publish(Publication((uint8_t)Topic::Server, (uint8_t)Message::EventServerRequest, resource));
  }


  // Publish the response code returned by the server.
  void ServerModule::_publishResponse(uint16_t code)
  {
    publish(Publication((uint8_t)Topic::Server, (uint8_t)Message::EventServerResponse, code));
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Resources (API endpoints)

  void ServerModule::_notFound()
  {
    _publishRequest();
    _sendResponse(404);
  }


  // GET /
  void ServerModule::_root()
  {
    _publishRequest();

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    JsonObject & response = jsonBuffer.createObject();
    response["version"] = kServerApiVersion;
    _sendResponse(200, response);
  }


  // POST /addpun
  void ServerModule::_addPun()
  {
    _publishRequest();

    if (!_server.hasArg(kServerPostRequestBodyArg))
    {
      _sendResponse(400, "Body not received.");
      return;
    }

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    JsonObject & body = jsonBuffer.parseObject(_server.arg(kServerPostRequestBodyArg));    
    if (!body.success())
    {
      _sendResponse(400, "Couldn't parse JSON.");
      return;
    }

    int16_t puns = body["puns"];
    if (puns == 0)
    {
      _sendResponse(400, "'puns' must be a non-zero integer.");
      return;
    }

    publish(Publication(
      (uint8_t)Topic::PunLevel,
      (uint8_t)Message::RequestPunAdd,
      puns));

    _sendResponse(200, "OK");
  }


  // GET /getpunlevel
  void ServerModule::_getPunLevel()
  {
    _publishRequest();

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    JsonObject & body = jsonBuffer.createObject();
    body["punLevel"] = _punLevel;
    _sendResponse(200, body);
  }


  // GET /getsettings
  void ServerModule::_getSettings()
  {
    _publishRequest();

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;

    JsonObject & body         = jsonBuffer.createObject();
    JsonObject & audio        = jsonBuffer.createObject();
    JsonObject & punLevel     = jsonBuffer.createObject();
    JsonObject & signalLight  = jsonBuffer.createObject();

    body[kSettingsAudioSection]       = audio;
    body[kSettingsPunLevelSection]    = punLevel;
    body[kSettingsSignalLightSection] = signalLight;

    audio[kSettingsAudioPlayDurationKey]          = _settings.audio.playDuration;
    audio[kSettingsAudioPlayCountKey]             = _settings.audio.playCount;
    audio[kSettingsAudioSampleFilenameKey]        = _settings.audio.sampleFilename;

    punLevel[kSettingsPunLevelPerPunKey]          = _settings.punLevel.perPun;
    punLevel[kSettingsPunLevelAlarmKey]           = _settings.punLevel.alarm;
    punLevel[kSettingsPunLevelMaxKey]             = _settings.punLevel.max;
    punLevel[kSettingsPunLevelRecoveryTimeKey]    = _settings.punLevel.recoveryTime;

    signalLight[kSettingsSignalLightDurationKey]  = _settings.signalLight.duration;

    _sendResponse(200, body);
  }


  // POST /setsettings
  void ServerModule::_setSettings()
  {
    _publishRequest();

    if (!_server.hasArg(kServerPostRequestBodyArg))
    {
      _sendResponse(400, "Body not received.");
      return;
    }

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    JsonObject & body = jsonBuffer.parseObject(_server.arg(kServerPostRequestBodyArg));

    if (!body.success())
    {
      _sendResponse(400, "Couldn't parse JSON.");
      return;
    }

    // Audio
    JsonObject & audio = body[kSettingsAudioSection];
    if (audio.success())
    {
      _setSetting(audio, kSettingsAudioSection, kSettingsAudioPlayDurationKey);
      _setSetting(audio, kSettingsAudioSection, kSettingsAudioPlayCountKey);
      _setSetting(audio, kSettingsAudioSection, kSettingsAudioSampleFilenameKey);
    }

    // Pun Level
    JsonObject & punLevel = body[kSettingsPunLevelSection];
    if (punLevel.success())
    {
      _setSetting(punLevel, kSettingsPunLevelSection, kSettingsPunLevelPerPunKey);
      _setSetting(punLevel, kSettingsPunLevelSection, kSettingsPunLevelAlarmKey);
      _setSetting(punLevel, kSettingsPunLevelSection, kSettingsPunLevelMaxKey);
      _setSetting(punLevel, kSettingsPunLevelSection, kSettingsPunLevelRecoveryTimeKey);
    }
    
    // Signal Light
    JsonObject & signalLight = body[kSettingsSignalLightSection];
    if (signalLight.success())
    {
      _setSetting(signalLight, kSettingsSignalLightSection, kSettingsSignalLightDurationKey);
    }

    // Save to the SD card.
    publish(Publication(
        (uint8_t)Topic::Settings,
        (uint8_t)Message::RequestSettingsSave));
    
    _sendResponse(200, "OK");
  }


  // POST /resetsettings
  void ServerModule::_resetSettings()
  {
    _publishRequest();
    
    // Reset and save to the SD card.
    publish(Publication(
        (uint8_t)Topic::Settings,
        (uint8_t)Message::RequestSettingsReset));

    _sendResponse(200, "OK");
  }


  // GET /listsamples
  void ServerModule::_listSamples()
  {
    _publishRequest();

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    JsonArray & body = jsonBuffer.createArray();

    for (uint8_t i = 0; i < _sampleCount; i++)
    {
      body.add(_samples[i]);
    }

    _sendResponse(200, body);
  }


  // POST /playsample
  void ServerModule::_playSample()
  {
    _publishRequest();
    
    if (!_server.hasArg(kServerPostRequestBodyArg))
    {
      _sendResponse(400, "Body not received.");
      return;
    }

    StaticJsonBuffer<kServerJsonBufferLength> jsonBuffer;
    JsonObject & body = jsonBuffer.parseObject(_server.arg(kServerPostRequestBodyArg));    
    if (!body.success())
    {
      _sendResponse(400, "Couldn't parse JSON.");
      return;
    }

    const char * sampleFilename = body["sampleFilename"];
    uint8_t len = strlen(sampleFilename);
    if (len == 0)
    {
      _sendResponse(400, "No 'sampleFilename' specified.");
      return;
    }
    if (len > 12)
    {
      _sendResponse(400, "'sampleFilename' must be in 8.3 filename format.");
      return;
    }

    publish(Publication(
      (uint8_t)Topic::Audio,
      (uint8_t)Message::RequestAudioSamplePlay,
      sampleFilename));

    _sendResponse(200, "OK");
  }
}