#pragma once

#include <ArduinoJson.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#include <PubSub.h>

#include "../_common/Enums/Message.h"
#include "../_common/Enums/Topic.h"
#include "../_common/Constants.h"
#include "../_common/Settings.h"


namespace pom
{
  ////////////////////////////////////////////////////////////////////////////////
  // Constants

  const char      kServerApiVersion[]         = "1.2";    // Refactored to pub/sub and added "/listsamples" endpoint
  const char      kServerPostRequestBodyArg[] = "plain";  // POST request body is accessible as an argument called "plain".
  const uint16_t  kServerPort                 = 80;
  const size_t    kServerJsonBufferLength     = 512;
  const size_t    kServerTextBufferLength     = 512;
  const size_t    kServerSamplesListMax       = 32;


  ////////////////////////////////////////////////////////////////////////////////
  // Class definition

  class ServerModule : public Publisher, public Subscriber
  {
  public:
    ServerModule  (Broker * pBroker);
    void begin    ();
    void loop     ();
    void notify   (Publication publication);

  private:
    ESP8266WebServer  _server;
    uint16_t          _punLevel;
    Settings          _settings;
    bool              _running;
    uint8_t           _sampleCount;
    char              _samples[kServerSamplesListMax][13]; // We only need 13 chars per sample (8.3 format + null terminator)

    // Event handlers
    void    _onEventWifiConnectionStatusChanged (uint8_t status);
    void    _onEventPunLevelChanged             (uint16_t punLevel);
    void    _onEventSettingChanged              (const char * sectionKeyValue);
    void    _onEventSettingsSamplesCleared      ();
    void    _onEventSettingsSampleAdded         (const char * filename);

    // Server helpers
    void    _sendResponse           (uint16_t code);
    void    _sendResponse           (uint16_t code, const char * message);
    void    _sendResponse           (uint16_t code, JsonObject & response);
    void    _sendResponse           (uint16_t code, JsonArray & response);
    void    _setSetting             (JsonObject & json, const char * section, const char * key);
    void    _publishRequest         ();
    void    _publishResponse        (uint16_t code);

    // Resources (API endpoints)
    void    _notFound               ();
    void    _root                   ();
    void    _addPun                 ();
    void    _getPunLevel            ();
    void    _getSettings            ();
    void    _setSettings            ();
    void    _resetSettings          ();
    void    _listSamples            ();
    void    _playSample             ();
  };
}