EESchema Schematic File Version 4
LIBS:pun-o-meter-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pun-o-meter-rescue:Conn_01x14 J8
U 1 1 5B486054
P 8700 1900
F 0 "J8" H 8700 2600 50  0000 C CNN
F 1 "TeensyL" H 8700 1100 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x14_Pitch2.54mm" H 8700 1900 50  0001 C CNN
F 3 "" H 8700 1900 50  0001 C CNN
	1    8700 1900
	-1   0    0    1   
$EndComp
$Comp
L pun-o-meter-rescue:Conn_01x14 J13
U 1 1 5B486145
P 7900 1800
F 0 "J13" H 7900 1000 50  0000 C CNN
F 1 "TeensyR" H 7900 2500 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x14_Pitch2.54mm" H 7900 1800 50  0001 C CNN
F 3 "" H 7900 1800 50  0001 C CNN
	1    7900 1800
	1    0    0    -1  
$EndComp
Text GLabel 8900 2400 2    50   Input ~ 0
ESPTX_TEENSYRX
Text GLabel 8900 2300 2    50   Output ~ 0
ESPRX_TEENSYTX
$Comp
L power:GND #PWR01
U 1 1 5B486378
P 9700 2500
F 0 "#PWR01" H 9700 2250 50  0001 C CNN
F 1 "GND" H 9700 2350 50  0000 C CNN
F 2 "" H 9700 2500 50  0001 C CNN
F 3 "" H 9700 2500 50  0001 C CNN
	1    9700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 2500 9700 2500
$Comp
L power:+5V #PWR02
U 1 1 5B4863AB
P 7300 2500
F 0 "#PWR02" H 7300 2350 50  0001 C CNN
F 1 "+5V" H 7300 2640 50  0000 C CNN
F 2 "" H 7300 2500 50  0001 C CNN
F 3 "" H 7300 2500 50  0001 C CNN
	1    7300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 2500 7700 2500
Text GLabel 7700 1900 0    50   Output ~ 0
AMP_SW
Text Notes 8000 2500 0    50   ~ 0
Vin
Text Notes 8000 2400 0    50   ~ 0
AGND
Text Notes 8000 2300 0    50   ~ 0
3.3V\n
Text Notes 8000 2200 0    50   ~ 0
23\n
Text Notes 8000 2100 0    50   ~ 0
22\n
Text Notes 8000 2000 0    50   ~ 0
21\n
Text Notes 8000 1900 0    50   ~ 0
20\n
Text Notes 8000 1800 0    50   ~ 0
19\n
Text Notes 8000 1700 0    50   ~ 0
18
Text Notes 8000 1600 0    50   ~ 0
17\n
Text Notes 8000 1500 0    50   ~ 0
16
Text Notes 8000 1400 0    50   ~ 0
15\n
Text Notes 8000 1300 0    50   ~ 0
14\n
Text Notes 8000 1200 0    50   ~ 0
13\n
Text Notes 8600 1200 2    50   ~ 0
12
Text Notes 8600 1300 2    50   ~ 0
11\n
Text Notes 8600 1400 2    50   ~ 0
10
Text Notes 8600 1500 2    50   ~ 0
9
Text Notes 8600 1600 2    50   ~ 0
8
Text Notes 8600 1700 2    50   ~ 0
7
Text Notes 8600 1800 2    50   ~ 0
6
Text Notes 8600 1900 2    50   ~ 0
5
Text Notes 8600 2000 2    50   ~ 0
4
Text Notes 8600 2100 2    50   ~ 0
3
Text Notes 8600 2200 2    50   ~ 0
2
Text Notes 8600 2300 2    50   ~ 0
1
Text Notes 8600 2400 2    50   ~ 0
0
Text Notes 8600 2500 2    50   ~ 0
GND
Text Notes 7800 800  0    50   ~ 0
Teensy 3.2 and Audio Board\n"POM.CORE"
Wire Bus Line
	7950 2550 8200 2550
Wire Bus Line
	8200 2650 8200 2500
Wire Bus Line
	8200 2650 8400 2650
Wire Bus Line
	8400 2650 8400 2500
Wire Bus Line
	8400 2550 8650 2550
Wire Bus Line
	8400 2500 8200 2500
Wire Bus Line
	7950 1150 8650 1150
Text GLabel 8900 2200 2    50   Output ~ 0
StepperA2
Text GLabel 8900 2100 2    50   Output ~ 0
StepperA1
Text GLabel 8900 2000 2    50   Output ~ 0
StepperB1
Text GLabel 8900 1900 2    50   Output ~ 0
StepperB2
NoConn ~ 8900 1700
NoConn ~ 8900 1200
NoConn ~ 7700 1300
NoConn ~ 8900 1400
NoConn ~ 7700 2300
NoConn ~ 7700 2400
NoConn ~ 8900 1300
NoConn ~ 8900 1500
NoConn ~ 8900 1800
NoConn ~ 7700 1200
NoConn ~ 7700 1400
NoConn ~ 7700 1700
NoConn ~ 7700 1800
NoConn ~ 7700 2100
NoConn ~ 7700 2200
Text Notes 7600 2230 2    30   ~ 0
Audio_LRCLK
Text Notes 7600 2130 2    30   ~ 0
Audio_TX
Text Notes 7600 1830 2    30   ~ 0
SCL
Text Notes 7600 1730 2    30   ~ 0
SDA
Text Notes 7600 1330 2    30   ~ 0
SPI_SCLK
Text Notes 7600 1230 2    30   ~ 0
Audio_RX
Text Notes 9000 1230 0    30   ~ 0
SPI_MISO
Text Notes 9000 1330 0    30   ~ 0
Audio_MCLK
Text Notes 9000 1430 0    30   ~ 0
SD_CS
Text Notes 9000 1530 0    30   ~ 0
Audio_BCLK
Text Notes 9000 1730 0    30   ~ 0
SPI_MOSI
Text Notes 9000 1830 0    30   ~ 0
MEM_CS
NoConn ~ 7700 2000
Text Notes 7600 1430 2    30   ~ 0
Audio_Vol
Text GLabel 8900 1600 2    50   Output ~ 0
SignalLightRelay
$Comp
L pun-o-meter-rescue:Conn_01x11 J12
U 1 1 5B486A2E
P 8300 5450
F 0 "J12" H 8300 6050 50  0000 C CNN
F 1 "Amp" H 8300 4850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x11_Pitch2.00mm" H 8300 5450 50  0001 C CNN
F 3 "" H 8300 5450 50  0001 C CNN
	1    8300 5450
	0    -1   -1   0   
$EndComp
NoConn ~ 8500 5650
Text GLabel 8400 5650 3    50   Input ~ 0
AMP_SW
$Comp
L power:GND #PWR03
U 1 1 5B486C08
P 8200 5950
F 0 "#PWR03" H 8200 5700 50  0001 C CNN
F 1 "GND" H 8200 5800 50  0000 C CNN
F 2 "" H 8200 5950 50  0001 C CNN
F 3 "" H 8200 5950 50  0001 C CNN
	1    8200 5950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5B486C1C
P 8400 6250
F 0 "#PWR04" H 8400 6100 50  0001 C CNN
F 1 "+5V" H 8400 6390 50  0000 C CNN
F 2 "" H 8400 6250 50  0001 C CNN
F 3 "" H 8400 6250 50  0001 C CNN
	1    8400 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 5650 8300 6250
Wire Wire Line
	8200 5650 8200 5950
Wire Bus Line
	7750 5400 7750 5000
Wire Bus Line
	7750 5000 8850 5000
Wire Bus Line
	8850 5000 8850 5400
Text Notes 8200 4900 0    50   ~ 0
Amp
$Comp
L pun-o-meter-rescue:Conn_01x10 J14
U 1 1 5B486EDC
P 8700 3900
F 0 "J14" H 8700 3300 50  0000 C CNN
F 1 "TB6612_IN" H 8700 4400 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x10_Pitch2.54mm" H 8700 3900 50  0001 C CNN
F 3 "" H 8700 3900 50  0001 C CNN
	1    8700 3900
	-1   0    0    1   
$EndComp
$Comp
L pun-o-meter-rescue:Conn_01x06 J9
U 1 1 5B486F11
P 7900 3800
F 0 "J9" H 7900 4300 50  0000 C CNN
F 1 "TB6612_OUT" H 7900 3200 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 7900 3800 50  0001 C CNN
F 3 "" H 7900 3800 50  0001 C CNN
	1    7900 3800
	1    0    0    -1  
$EndComp
Wire Bus Line
	8650 4350 7850 4350
Wire Bus Line
	7850 4350 7850 4150
Wire Bus Line
	7850 3550 7850 3350
Wire Bus Line
	7850 3350 8650 3350
Text Notes 7900 3200 0    50   ~ 0
Stepper Motor Driver
Text Notes 8600 4300 2    50   ~ 0
VM
Text Notes 8600 4200 2    50   ~ 0
VCC
Text Notes 8600 4100 2    50   ~ 0
GND
Text Notes 8600 4000 2    50   ~ 0
PWMB
Text Notes 8600 3900 2    50   ~ 0
BIN2
Text Notes 8600 3800 2    50   ~ 0
BIN1
Text Notes 8600 3700 2    50   ~ 0
STBY
Text Notes 8600 3500 2    50   ~ 0
AIN2
Text Notes 8600 3600 2    50   ~ 0
AIN1
Text Notes 8600 3400 2    50   ~ 0
PWMA
Text Notes 8000 4050 0    50   ~ 0
MOTORA
Text Notes 8000 3650 0    50   ~ 0
MOTORB
Text Notes 8000 3850 0    50   ~ 0
GND
Wire Bus Line
	7950 3750 8200 3750
Wire Bus Line
	8200 3750 8200 3950
Wire Bus Line
	8200 3950 7950 3950
$Comp
L power:+5V #PWR05
U 1 1 5B487751
P 9500 3400
F 0 "#PWR05" H 9500 3250 50  0001 C CNN
F 1 "+5V" H 9500 3540 50  0000 C CNN
F 2 "" H 9500 3400 50  0001 C CNN
F 3 "" H 9500 3400 50  0001 C CNN
	1    9500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 4300 8900 4300
Wire Wire Line
	9500 4200 8900 4200
Wire Wire Line
	9500 3400 9500 4000
Connection ~ 9500 3400
$Comp
L power:GND #PWR06
U 1 1 5B4877E6
P 9700 4100
F 0 "#PWR06" H 9700 3850 50  0001 C CNN
F 1 "GND" H 9700 3950 50  0000 C CNN
F 2 "" H 9700 4100 50  0001 C CNN
F 3 "" H 9700 4100 50  0001 C CNN
	1    9700 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 4100 9700 4100
Wire Wire Line
	9500 4000 8900 4000
Connection ~ 9500 4200
Wire Wire Line
	9500 3400 8900 3400
Connection ~ 9500 4000
Text GLabel 8900 3800 2    50   Input ~ 0
StepperB1
Text GLabel 8900 3900 2    50   Input ~ 0
StepperB2
Text GLabel 8900 3500 2    50   Input ~ 0
StepperA2
Text GLabel 8900 3600 2    50   Input ~ 0
StepperA1
$Comp
L power:GND #PWR07
U 1 1 5B487611
P 6700 3200
F 0 "#PWR07" H 6700 2950 50  0001 C CNN
F 1 "GND" H 6700 3050 50  0000 C CNN
F 2 "" H 6700 3200 50  0001 C CNN
F 3 "" H 6700 3200 50  0001 C CNN
	1    6700 3200
	1    0    0    -1  
$EndComp
$Comp
L pun-o-meter-rescue:Conn_01x06 J7
U 1 1 5B487AB1
P 6500 3000
F 0 "J7" V 6500 3300 50  0000 C CNN
F 1 "Stepper" V 6600 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 6500 3000 50  0001 C CNN
F 3 "" H 6500 3000 50  0001 C CNN
	1    6500 3000
	0    1    -1   0   
$EndComp
Wire Wire Line
	7700 4100 6200 4100
Wire Wire Line
	6300 4000 7700 4000
Wire Wire Line
	7700 3700 7200 3700
Wire Wire Line
	7200 3700 7200 3900
Wire Wire Line
	7200 3900 6400 3900
Wire Wire Line
	7700 3600 7100 3600
Wire Wire Line
	7100 3600 7100 3800
Wire Wire Line
	6600 3700 6700 3700
$Comp
L power:+5V #PWR08
U 1 1 5B487E3B
P 6700 3700
F 0 "#PWR08" H 6700 3550 50  0001 C CNN
F 1 "+5V" H 6700 3840 50  0000 C CNN
F 2 "" H 6700 3700 50  0001 C CNN
F 3 "" H 6700 3700 50  0001 C CNN
	1    6700 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3900 7500 3900
Wire Wire Line
	7500 3900 7500 3800
Wire Wire Line
	7400 3800 7500 3800
Connection ~ 7500 3800
$Comp
L power:GND #PWR09
U 1 1 5B488703
P 7400 3800
F 0 "#PWR09" H 7400 3550 50  0001 C CNN
F 1 "GND" H 7400 3650 50  0000 C CNN
F 2 "" H 7400 3800 50  0001 C CNN
F 3 "" H 7400 3800 50  0001 C CNN
	1    7400 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 6250 8400 6250
$Comp
L pun-o-meter-rescue:Conn_01x15 J6
U 1 1 5B488CF3
P 3000 1700
F 0 "J6" H 3000 900 50  0000 C CNN
F 1 "ESP_R" H 3000 2500 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x15_Pitch2.54mm" H 3000 1700 50  0001 C CNN
F 3 "" H 3000 1700 50  0001 C CNN
	1    3000 1700
	0    1    1    0   
$EndComp
Text Notes 2450 2400 1    50   ~ 0
GND
Text Notes 2350 2400 1    50   ~ 0
3V3
Text Notes 2550 2400 1    50   ~ 0
TX
Text Notes 2650 2400 1    50   ~ 0
RX
Text Notes 2750 2400 1    50   ~ 0
D8
Text Notes 2850 2400 1    50   ~ 0
D7
Text Notes 2950 2400 1    50   ~ 0
D6
Text Notes 3050 2400 1    50   ~ 0
D5
Text Notes 3150 2400 1    50   ~ 0
GND
Text Notes 3250 2400 1    50   ~ 0
3V3
Text Notes 3350 2400 1    50   ~ 0
D4
Text Notes 3450 2400 1    50   ~ 0
D3
Text Notes 3550 2400 1    50   ~ 0
D2
Text Notes 3650 2400 1    50   ~ 0
D1
Text Notes 3750 2400 1    50   ~ 0
D0
Text Notes 2450 1800 3    50   ~ 0
GND
Text Notes 2350 1800 3    50   ~ 0
Vin
Text Notes 2550 1800 3    50   ~ 0
RST
Text Notes 2650 1800 3    50   ~ 0
EN
Text Notes 2750 1800 3    50   ~ 0
3V3
Text Notes 2850 1800 3    50   ~ 0
GND
Text Notes 2950 1800 3    50   ~ 0
CLK
Text Notes 3050 1800 3    50   ~ 0
SD0
Text Notes 3150 1800 3    50   ~ 0
CMD
Text Notes 3250 1800 3    50   ~ 0
SD1
Text Notes 3350 1800 3    50   ~ 0
SD2
Text Notes 3450 1800 3    50   ~ 0
SD3
Text Notes 3550 1800 3    50   ~ 0
RSV
Text Notes 3650 1800 3    50   ~ 0
RSV
Text Notes 3750 1800 3    50   ~ 0
A0
$Comp
L power:+5V #PWR010
U 1 1 5B489629
P 2300 1200
F 0 "#PWR010" H 2300 1050 50  0001 C CNN
F 1 "+5V" H 2300 1340 50  0000 C CNN
F 2 "" H 2300 1200 50  0001 C CNN
F 3 "" H 2300 1200 50  0001 C CNN
	1    2300 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 1500 2300 1200
Wire Wire Line
	2400 1500 2400 950 
Text GLabel 2500 2700 3    50   Output ~ 0
ESPTX_TEENSYRX
Text GLabel 2600 2700 3    50   Input ~ 0
ESPRX_TEENSYTX
$Comp
L Device:R R1
U 1 1 5B4899F7
P 3700 3050
F 0 "R1" V 3780 3050 50  0000 C CNN
F 1 "168" V 3700 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 3630 3050 50  0001 C CNN
F 3 "" H 3700 3050 50  0001 C CNN
	1    3700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 2700 3700 2900
Wire Wire Line
	3700 3200 3700 3800
$Comp
L pun-o-meter-rescue:Conn_01x02 J1
U 1 1 5B489AD9
P 3700 4000
F 0 "J1" H 3700 3800 50  0000 C CNN
F 1 "WIFI_LED" H 3700 4100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3700 4000 50  0001 C CNN
F 3 "" H 3700 4000 50  0001 C CNN
	1    3700 4000
	0    1    1    0   
$EndComp
Text Notes 3750 3900 1    30   ~ 0
-
Text Notes 3600 3900 1    30   ~ 0
+
NoConn ~ 3400 2700
NoConn ~ 3300 2700
NoConn ~ 3000 2700
NoConn ~ 2900 2700
NoConn ~ 2800 2700
NoConn ~ 2700 2700
NoConn ~ 2500 1500
NoConn ~ 2600 1500
NoConn ~ 2900 1500
NoConn ~ 3000 1500
NoConn ~ 3100 1500
NoConn ~ 3200 1500
NoConn ~ 3300 1500
NoConn ~ 3400 1500
NoConn ~ 3500 1500
NoConn ~ 3600 1500
NoConn ~ 3700 1500
Text Notes 3400 850  2    50   ~ 0
ESP-12E (ESP8266)\n"POM.API"
Text GLabel 4400 4850 2    50   Input ~ 0
SignalLightRelay
Wire Wire Line
	6200 4100 6200 3200
Wire Wire Line
	6300 4000 6300 3200
Wire Wire Line
	6400 3900 6400 3200
Wire Wire Line
	6500 3800 6500 3200
Wire Wire Line
	6600 3200 6600 3700
Wire Wire Line
	7100 3800 6500 3800
Wire Wire Line
	8100 5650 8100 6000
Wire Wire Line
	8000 5650 8000 6000
Wire Wire Line
	7900 5650 7900 6000
Wire Wire Line
	7800 6000 7800 5650
Text Label 8100 6000 1    30   ~ 0
AMP_SPK_L+
Text Label 8000 6000 1    30   ~ 0
AMP_SPK_L-
Text Label 7900 6000 1    30   ~ 0
AMP_SPK_R-
Text Label 7800 6000 1    30   ~ 0
AMP_SPK_R+
$Comp
L Device:R R2
U 1 1 5B48C83B
P 4150 4850
F 0 "R2" V 4230 4850 50  0000 C CNN
F 1 "1K" V 4150 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 4080 4850 50  0001 C CNN
F 3 "" H 4150 4850 50  0001 C CNN
	1    4150 4850
	0    -1   1    0   
$EndComp
Wire Wire Line
	4400 4850 4300 4850
$Comp
L pun-o-meter-rescue:BC548 Q1
U 1 1 5B48CAAD
P 4000 5250
F 0 "Q1" H 4200 5325 50  0000 L CNN
F 1 "BC548" H 4200 5250 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 4200 5175 50  0001 L CIN
F 3 "" H 4000 5250 50  0001 L CNN
	1    4000 5250
	0    -1   1    0   
$EndComp
Wire Wire Line
	4000 5050 4000 4850
$Comp
L power:GND #PWR011
U 1 1 5B48CBA6
P 4400 5350
F 0 "#PWR011" H 4400 5100 50  0001 C CNN
F 1 "GND" H 4400 5200 50  0000 C CNN
F 2 "" H 4400 5350 50  0001 C CNN
F 3 "" H 4400 5350 50  0001 C CNN
	1    4400 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 5350 4400 5350
$Comp
L power:+5V #PWR012
U 1 1 5B48CECE
P 4400 5850
F 0 "#PWR012" H 4400 5700 50  0001 C CNN
F 1 "+5V" H 4400 5990 50  0000 C CNN
F 2 "" H 4400 5850 50  0001 C CNN
F 3 "" H 4400 5850 50  0001 C CNN
	1    4400 5850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3400 5850 3600 5850
$Comp
L Device:D D1
U 1 1 5B48CFE5
P 3600 5600
F 0 "D1" H 3600 5700 50  0000 C CNN
F 1 "D" H 3600 5500 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P7.62mm_Horizontal" H 3600 5600 50  0001 C CNN
F 3 "" H 3600 5600 50  0001 C CNN
	1    3600 5600
	0    1    -1   0   
$EndComp
Wire Wire Line
	3600 5850 3600 5750
Wire Wire Line
	3600 5450 3600 5350
Wire Wire Line
	3400 5350 3600 5350
Connection ~ 3600 5850
$Comp
L pun-o-meter-rescue:Screw_Terminal_01x02 J3
U 1 1 5B48D77A
P 1550 7050
F 0 "J3" H 1550 6850 50  0000 C CNN
F 1 "12V_IN" H 1750 7000 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 1550 7050 50  0001 C CNN
F 3 "" H 1550 7050 50  0001 C CNN
	1    1550 7050
	-1   0    0    1   
$EndComp
$Comp
L pun-o-meter-rescue:Screw_Terminal_01x02 J2
U 1 1 5B48DA14
P 1550 6550
F 0 "J2" H 1550 6350 50  0000 C CNN
F 1 "12V_PILOT_LAMP" H 1950 6500 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 1550 6550 50  0001 C CNN
F 3 "" H 1550 6550 50  0001 C CNN
	1    1550 6550
	-1   0    0    1   
$EndComp
$Comp
L pun-o-meter-rescue:Screw_Terminal_01x02 J4
U 1 1 5B48DAC7
P 1550 6050
F 0 "J4" H 1550 5850 50  0000 C CNN
F 1 "12V_SIGNAL_LIGHT" H 2000 6000 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 1550 6050 50  0001 C CNN
F 3 "" H 1550 6050 50  0001 C CNN
	1    1550 6050
	-1   0    0    1   
$EndComp
Text Notes 1450 6450 0    30   ~ 0
+
Text Notes 1450 6950 0    30   ~ 0
+
Text Notes 1450 5950 0    30   ~ 0
+
Text Notes 1450 6600 0    30   ~ 0
-
Text Notes 1450 7100 0    30   ~ 0
-
Text Notes 1450 6100 0    30   ~ 0
-
Text Label 2100 6950 0    30   ~ 0
+12V
Connection ~ 3600 5350
$Comp
L pun-o-meter-rescue:Screw_Terminal_01x02 J11
U 1 1 5B4910A1
P 8100 6200
F 0 "J11" H 8500 6150 50  0000 C CNN
F 1 "SPK_L" H 8300 6150 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 8100 6200 50  0001 C CNN
F 3 "" H 8100 6200 50  0001 C CNN
	1    8100 6200
	0    1    1    0   
$EndComp
$Comp
L pun-o-meter-rescue:Screw_Terminal_01x02 J10
U 1 1 5B49122D
P 7900 6200
F 0 "J10" H 8300 6150 50  0000 C CNN
F 1 "SPK_R" H 8100 6150 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 7900 6200 50  0001 C CNN
F 3 "" H 7900 6200 50  0001 C CNN
	1    7900 6200
	0    1    1    0   
$EndComp
$Comp
L pun-o-meter-rescue:Conn_01x03 J15
U 1 1 5B49210C
P 8700 6200
F 0 "J15" H 9300 6200 50  0000 C CNN
F 1 "Amp_Line_In" H 9000 6200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 8700 6200 50  0001 C CNN
F 3 "" H 8700 6200 50  0001 C CNN
	1    8700 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	8600 6000 8600 5650
Wire Wire Line
	8700 5650 8700 6000
Wire Wire Line
	8800 6000 8800 5650
Text Label 8600 6000 1    30   ~ 0
AMP_LIN
Text Label 8700 6000 1    30   ~ 0
AMP_GNDA
Text Label 8800 6000 1    30   ~ 0
AMP_RIN
Text Notes 3050 4950 2    50   ~ 0
Signal Light
Wire Bus Line
	2300 2000 2150 2000
Wire Bus Line
	2150 2000 2150 2200
Wire Bus Line
	2150 2200 2300 2200
Wire Bus Line
	2300 2200 2300 2000
Wire Bus Line
	2250 1750 2250 2000
Wire Bus Line
	2250 2200 2250 2450
Wire Bus Line
	3750 1750 3750 2450
Text Notes 7900 5350 0    60   ~ 0
Jaycar XC4448\n(uses PAM8403 IC)\n2 x 3W
$Comp
L pun-o-meter-rescue:DPDT_Relay_2_Form_C RL1
U 1 1 5B5F8DA3
P 2800 5950
F 0 "RL1" H 2800 5200 60  0000 C CNN
F 1 "EC2-5NU" H 2800 6700 60  0000 C CNN
F 2 "RetroJDM_Power:EC2" H 2700 5250 60  0001 C CNN
F 3 "" H 2700 5250 60  0001 C CNN
	1    2800 5950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1750 6050 1950 6050
Wire Wire Line
	1950 6050 1950 6550
Wire Wire Line
	1950 7050 1750 7050
Wire Wire Line
	1750 6550 1950 6550
Connection ~ 1950 6550
Wire Wire Line
	1750 6950 1850 6950
Wire Wire Line
	1850 6950 1850 6450
Wire Wire Line
	1850 6450 1750 6450
Wire Wire Line
	3600 6950 3600 6050
Wire Wire Line
	3600 6050 3400 6050
Connection ~ 1850 6950
Text Label 1750 5950 0    30   ~ 0
+12V_SWITCHED
Text Label 1950 6400 1    30   ~ 0
GROUND
NoConn ~ 3400 6450
NoConn ~ 2200 6550
NoConn ~ 2200 6350
Wire Wire Line
	3600 3700 3600 3800
Wire Wire Line
	2400 2700 2400 3500
Wire Wire Line
	2300 2700 2300 3700
Wire Wire Line
	3100 3500 3100 2700
Wire Wire Line
	2400 3500 2750 3500
$Comp
L power:+3V3 #PWR013
U 1 1 5B655250
P 2100 3700
F 0 "#PWR013" H 2100 3550 50  0001 C CNN
F 1 "+3V3" H 2100 3840 50  0000 C CNN
F 2 "" H 2100 3700 50  0001 C CNN
F 3 "" H 2100 3700 50  0001 C CNN
	1    2100 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5B655297
P 2750 3500
F 0 "#PWR014" H 2750 3250 50  0001 C CNN
F 1 "GND" H 2750 3350 50  0000 C CNN
F 2 "" H 2750 3500 50  0001 C CNN
F 3 "" H 2750 3500 50  0001 C CNN
	1    2750 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1500 2700 1200
Wire Wire Line
	2800 1500 2800 950 
$Comp
L power:GND #PWR015
U 1 1 5B6556F4
P 2900 950
F 0 "#PWR015" H 2900 700 50  0001 C CNN
F 1 "GND" H 2900 800 50  0000 C CNN
F 2 "" H 2900 950 50  0001 C CNN
F 3 "" H 2900 950 50  0001 C CNN
	1    2900 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR016
U 1 1 5B65573B
P 2700 1200
F 0 "#PWR016" H 2700 1050 50  0001 C CNN
F 1 "+3V3" H 2700 1340 50  0000 C CNN
F 2 "" H 2700 1200 50  0001 C CNN
F 3 "" H 2700 1200 50  0001 C CNN
	1    2700 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2700 3200 3700
Connection ~ 3200 3700
Connection ~ 2800 950 
Wire Wire Line
	2100 3700 2300 3700
Wire Wire Line
	2400 950  2800 950 
Text Label 3700 3600 1    30   ~ 0
WIFI_LED_R1
Text Label 3700 2900 1    30   ~ 0
R1_D0
Text Label 3600 5350 0    30   ~ 0
RL1_NEG
Text Label 3650 5850 0    30   ~ 0
RL1_POS
Text Label 4000 5050 1    30   ~ 0
Q1_R2
Text Label 6400 3700 1    30   ~ 0
STEP_A1
Text Label 6500 3700 1    30   ~ 0
STEP_A2
Text Label 6300 3700 1    30   ~ 0
STEP_B1
Text Label 6200 3700 1    30   ~ 0
STEP_B2
Wire Wire Line
	2200 6150 2050 6150
Wire Wire Line
	2050 6150 2050 5950
Wire Wire Line
	2050 5950 1750 5950
NoConn ~ 2200 5950
Wire Wire Line
	8900 3700 9700 3700
Wire Wire Line
	9700 3700 9700 4100
Connection ~ 9700 4100
$Comp
L pun-o-meter-rescue:Conn_01x03 J16
U 1 1 5B8322FD
P 6400 2100
F 0 "J16" H 7000 2100 50  0000 C CNN
F 1 "Remote_Buttons" V 6500 2100 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03_Pitch2.54mm" H 6400 2100 50  0001 C CNN
F 3 "" H 6400 2100 50  0001 C CNN
	1    6400 2100
	0    -1   1    0   
$EndComp
Wire Wire Line
	7700 1500 6300 1500
Wire Wire Line
	6300 1500 6300 1900
Wire Wire Line
	7700 1600 6500 1600
Wire Wire Line
	6500 1600 6500 1900
Connection ~ 2300 3700
Text Label 6300 1500 0    30   ~ 0
BTN_ADD_PUN
Text Label 6500 1600 0    30   ~ 0
BTN_REMOVE_PUN
$Comp
L power:GND #PWR017
U 1 1 5B83493D
P 6900 1900
F 0 "#PWR017" H 6900 1650 50  0001 C CNN
F 1 "GND" H 6900 1750 50  0000 C CNN
F 2 "" H 6900 1900 50  0001 C CNN
F 3 "" H 6900 1900 50  0001 C CNN
	1    6900 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1900 6900 1800
Wire Wire Line
	6900 1800 6400 1800
Wire Wire Line
	6400 1800 6400 1900
Wire Wire Line
	9500 4200 9500 4300
Wire Wire Line
	9500 4000 9500 4200
Wire Wire Line
	7500 3800 7700 3800
Wire Wire Line
	3600 5850 4400 5850
Wire Wire Line
	3600 5350 3800 5350
Wire Wire Line
	1950 6550 1950 7050
Wire Wire Line
	1850 6950 3600 6950
Wire Wire Line
	3200 3700 3600 3700
Wire Wire Line
	2800 950  2900 950 
Wire Wire Line
	2750 3500 3100 3500
Wire Wire Line
	2300 3700 3200 3700
$Comp
L pun-o-meter-rescue:Conn_01x15 J5
U 1 1 5B488C2F
P 3000 2500
F 0 "J5" H 3000 3300 50  0000 C CNN
F 1 "ESP_L" H 3000 1700 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x15_Pitch2.54mm" H 3000 2500 50  0001 C CNN
F 3 "" H 3000 2500 50  0001 C CNN
	1    3000 2500
	0    -1   -1   0   
$EndComp
NoConn ~ 3500 2700
NoConn ~ 3600 2700
Connection ~ 2750 3500
$EndSCHEMATC
