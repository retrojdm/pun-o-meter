# Pun-O-Meter
https://bitbucket.org/retrojdm/pun-o-meter


## Summary
The Pun-O-Meter can function via either button inputs or an API.

The user can add a set amount to a "pun level" shown on a gauge via a stepper motor.

When the pun level reaches a critical value, an alarm sound and signal light are activated. The pun level decays over time.

Settings are stored on an SD card, and can be edited directly, or via the API.

WIFI settings are stored in a separate file on the SD card.


# Hardware
* PJRC Teensy 3.2
* PJRC Teensy audio board
* ESP-12E module
* Switec X27.168 stepper motor and TB6612 driver board
* 12V rotating signal light (on a separate power supply) controlled via a 5V relay.
* Status LED


# Architecture
The Pun-o-Meter is divided into two boards. Each with their own microcontroller: POM.Core and POM.Api.

Neither board is a master or slave. Instead, a "publisher/subscriber" design pattern is used.

Each module publishes events and subscribes to "topics" of events they're intereseted in.

The "broker" on each board facilitates the distribution of the event messages.

The "Serial" module on each board passes the event messages to the other, meaning the modules can behave as though they were on a single board.

### POM.Core (Teensy 3.2 + Audio board)
* **Audio** *(play wav samples stored on the SD card)*
* **Input** *(generate button and potentiometer events)*
* **PunLevel** *(keep track of pun level, and generate alerts)*
* **Serial** *(pass event messages to/from POM.Api)*
* **SerialMonitor** *(Show pub/sub publications on a serial monitor connected to the USB port)*
* **Settings** *(read/write "settings.json" and read "wifi.json" file)*
* **SignalLight** *(switch the rotating signal light on/off)*
* **Stepper** *(show the pun level on a gauge via a stepper motor)*

### POM.Api (ESP-12E)
* **Led** *(show status via a blinking "status" LED)*
* **Serial** *(pass event messages to/from POM.Core)*
* **Server** *(Handle GET and POST requests via the WIFI API)*
* **Wifi** *(connect to an access point)*


# Changelog
* **v1.2:** Added "/playsample" API endpoint.
* **v1.1:** Refactored code into publisher/subscriber design pattern and added "/listsamples" API endpont.
* **v1.0:** Added API
* **v0.9:** Basic functionality: add/remove pun via physical button